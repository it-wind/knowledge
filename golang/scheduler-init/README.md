### 1.简单调度概述

```text
M:N 线程模型
*M个goroutine运行在N个操作系统线程之上
*内核负责对这N个操作系统线程进行调度
*N个系统线程负责对M个goroutine进行调度和运行
```

```text
何为调度：
*程序代码按照一定的算法在适当的时候挑选出合适的goroutine并放到CPU上去运行的过程
```

```go
//简单调度示例
// 程序启动时的初始化代码 (略)

// 创建N个操作系统线程执行schedule函数
for i := 0; i < N; i++ {
    clone(schedule) // 创建一个操作系统线程执行schedule函数
}

//schedule函数实现调度逻辑
func schedule() {
   for { //调度循环
         // 根据某种算法从M个goroutine中找出一个需要运行的goroutine
         g := find_runnable_g()
         run_g(g) // CPU运行该goroutine，直到需要调度其它goroutine才返回
         save_g(g) // 保存goroutine的状态，主要是寄存器的值
    }
}
```

```go
//***利用tls绑定私有m***
//***加入本地队列p***

// 程序启动时的初始化代码 (略)

type g struct {}

type p struct {
    runq []*g //存放多个g的队列
    m *m
}

type m stuct {
	tls uintptr //利用fs段和系统线程绑定
	p *p //m包含p，p包含一个本地队列
	curg *g
}

// 创建N个操作系统线程执行schedule函数
for i := 0; i < N; i++ {
     clone(schedule) // 创建一个操作系统线程执行schedule函数
}

//初始化m
func initm() *m {
	mp := &m{}
	mp.p = getp(mp) //m和p进行绑定
	set_os_fs(&mp.tls) //设置线程的fs段为&m.tls，相当于m和线程进行绑定
	return mp
}

//schedule函数实现调度逻辑
func schedule() {
    // 创建和初始化m, 绑定本地队列， m与线程绑定
    mp := initm()   
    for { //调度循环
    	if len(mp.p.runq) == 0 {
            // 根据某种算法从全局运行队列中找出一个需要运行的goroutine
    	    g := find_runnable_g_global()
        } else {
            // 根据某种算法从私有的局部运行队列中找出一个需要运行的goroutine
        	g := find_runnable_g()
        }
        mp.curg = g //m与g绑定
        run_g(g) // CPU运行该goroutine，直到需要调度其它goroutine才返回
        save_g(g) // 保存goroutine的状态，主要是寄存器的值
     }
}
```

g、p、m关系图
![img.jpg](img.jpg)
```text
m:每个工作线程都有唯一的一个m结构体的实例对象与之对应
p:每个m都绑定了一个p，每个p都有一个私有的本地goroutine队列
g:保存CPU寄存器的值以及goroutine的其它一些状态信息
schet:保存调度器自身的状态信息，保存全局goroutine的运行队列
```

*重要数据结构
```go
//用于记录goroutine使用的栈的起始和结束位置
type stack struct {  
    lo uintptr    // 栈顶，指向内存低地址
    hi uintptr    // 栈底，指向内存高地址
}

//gobuf结构体用于保存goroutine的调度信息，主要包括CPU的几个寄存器的值
type gobuf struct {
    sp   uintptr  // 保存CPU的rsp寄存器的值
    pc   uintptr  // 保存CPU的rip寄存器的值
    g    guintptr // 记录当前这个gobuf对象属于哪个goroutine
 
    // 保存系统调用的返回值，因为从系统调用返回之后如果p被其它工作线程抢占，
    // 则这个goroutine会被放入全局运行队列被其它工作线程调度，其它线程需要知道系统调用的返回值。
    ret  sys.Uintreg
 
    // 保存CPU的rbp寄存器的值
    bp   uintptr // for GOEXPERIMENT=framepointer
}

// 前文所说的g，它代表了一个goroutine
type g struct {
    // 记录该goroutine使用的栈
    stack       stack   // offset known to runtime/cgo
    
    m              *m
    
    // 保存调度信息，主要是几个寄存器的值
    sched          gobuf
    
    //指向当前队列的下一个g
    //全局队列、p中的空闲队列
    schedlink      guintptr
}

const tlsSlots = 6

type m struct {
    // g0主要用来记录工作线程使用的栈信息，在执行调度代码时需要使用这个栈
    // 执行用户goroutine代码时，使用用户goroutine自己的栈，调度时会发生栈的切换
    g0      *g     // goroutine with scheduling stack

    // 通过TLS实现m结构体对象与工作线程之间的绑定
    tls           [tlsSlots]uintptr   // thread-local storage (for x86 extern register)

    // 指向工作线程正在运行的goroutine的g结构体对象
    curg          *g       // current running goroutine
 
    // 记录与当前工作线程绑定的p结构体对象
    p             puintptr // attached p for executing go code (nil if not executing go code)
   
    // spinning状态：表示当前工作线程正在试图从其它工作线程的本地运行队列偷取goroutine
    spinning      bool // m is out of work and is actively looking for work
    
    // m.alllink = allm
    alllink       *m
}

type p struct {
    status       uint32 // one of pidle/prunning/...

    m                muintptr
    
    //本地goroutine运行队列
    runqhead uint32  // 队列头
    runqtail uint32     // 队列尾
    runq     [256]guintptr  //使用数组实现的循环队列

    runnext guintptr

    // Available G's (status == Gdead)
    //gList.head为头部, 利用g中的schedlink链接下一个空闲的g
    gFree struct {
        gList
        n int32
    }
}

//schedt结构体用来保存调度器的状态信息和goroutine的全局运行队列
type schedt struct {
    // 由空闲的m
    midle        muintptr // idle m's waiting for work
    // 空闲的m的数量
    nmidle       int32    // number of idle m's waiting for work

    //用于构建下一个m的id, 自增
    mnext        int64    // number of m's that have been created and next M ID
    
    // 最多只能创建maxmcount个工作线程
    maxmcount    int32    // maximum number of m's allowed (or die)

    // 由空闲的p结构体对象组成的链表
    pidle      puintptr // idle p's
    // 空闲的p结构体对象的数量
    npidle     uint32

    // Global runnable queue.
    // goroutine全局运行队列
    runq     gQueue
    runqsize int32

    // Global cache of dead G's.
    // gFree是所有已经退出的goroutine对应的g结构体对象组成的链表
    // 用于缓存g结构体对象，避免每次创建goroutine时都重新分配内存
    gFree struct {
        lock          mutex
        stack        gList // Gs with stacks
        noStack   gList // Gs without stacks
        n              int32
    }
}
```

*重要的全局变量
```go
allgs     []*g     // 保存所有的g #runtime/proc.go:50
allm       *m    // 所有的m构成的一个链表，包括下面的m0 #runtime/runtime2.go:1091
allp       []*p    // 保存所有的p，len(allp) == gomaxprocs #runtime/runtime2.go:1103

ncpu             int32   // 系统中cpu核的数量，程序启动时由runtime代码初始化 #runtime/runtime2.go:1093
gomaxprocs int32   // p的最大值，默认等于ncpu，但可以通过GOMAXPROCS修改 #runtime/runtime2.go:1092

sched      schedt     // 调度器结构体对象，记录了调度器的工作状态 #runtime/runtime2.go:1095

m0  m       // 关联主线程 #runtime/proc.go:114
g0   g        // m0的g0，也就是m0.g0 = &g0 #runtime/proc.go:115
```

### 2.调度器初始化
程序被操作系统加载的经过的几个顺序
```text
1.从磁盘上把可执行程序读入内存；

2.创建进程和主线程；

3.为主线程分配栈空间；

4.把由用户在命令行输入的参数拷贝到主线程的栈；

5.把主线程放入操作系统的运行队列等待被调度执起来运行；
```

hello.go代码如下
```go
package main

import "fmt"

func main() {
    fmt.Println("Hello World!")
}
```

linux系统下开始调试
使用调试用具：dlv (dlv教程)[https://gitee.com/it-wind/knowledge/tree/master/golang/godev#5%E7%A8%8B%E5%BA%8F%E7%9A%84%E5%85%A5%E5%8F%A3]
使用readelf读取程序的入口点Entry point address

```text
//说明： --gcflags="-N -l"禁用编译器的优化和内联
[root@78aa7d637940 scheduler-init]# go build --gcflags="-N -l" hello.go
//说明： 使用readelf读取程序的入口点Entry point address：0x45ee80
[root@78aa7d637940 scheduler-init]# readelf -h hello
ELF Header:
  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00
  Class:                             ELF64
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              EXEC (Executable file)
  Machine:                           Advanced Micro Devices X86-64
  Version:                           0x1
  Entry point address:               0x45ee80
  Start of program headers:          64 (bytes into file)
  Start of section headers:          456 (bytes into file)
  Flags:                             0x0
  Size of this header:               64 (bytes)
  Size of program headers:           56 (bytes)
  Number of program headers:         7
  Size of section headers:           64 (bytes)
  Number of section headers:         23
  Section header string table index: 3

[root@78aa7d637940 scheduler-init]# dlv exec ./hello
Type 'help' for list of commands.
//说明： 这里设置断点为：0x45ee80
(dlv) b *0x45ee80
//说明： 这里可以看到程序的入口函数 _rt0_amd64_linux() 
> _rt0_amd64_linux() /usr/lib/go/src/runtime/rt0_linux_amd64.s:8 (hits total:1) (PC: 0x45ee80)
Warning: debugging optimized function
     3: // license that can be found in the LICENSE file.
     4:
     5: #include "textflag.h"
     6:
     7: TEXT _rt0_amd64_linux(SB),NOSPLIT,$-8
=>   8:         JMP     _rt0_amd64(SB)    //说明：跳转到 _rt0_amd64()

> _rt0_amd64() /usr/lib/go/src/runtime/asm_amd64.s:16 (hits total:1) (PC: 0x45b460)
    11: // _rt0_amd64 is common startup code for most amd64 systems when using
    12: // internal linking. This is the entry point for the program from the
    13: // kernel for an ordinary -buildmode=exe program. The stack holds the
    14: // number of arguments and the C-style argv.
    15: TEXT _rt0_amd64(SB),NOSPLIT,$-8
=>  16:         MOVQ    0(SP), DI       // argc 把argc装入DI寄存器
    17:         LEAQ    8(SP), SI       // argv 把argv首地址装入SI寄存器
    18:         JMP     runtime·rt0_go(SB)    //跳转到runtime·rt0_go
    19:
    20: // main is common startup code for most amd64 systems when using
    21: // external linking. The C startup code will call the symbol "main"

> runtime.rt0_go() /usr/lib/go/src/runtime/asm_amd64.s:161 (hits total:1) (PC: 0x45b480)
TEXT runtime·rt0_go(SB),NOSPLIT|TOPFRAME,$0
	// copy arguments forward on an even stack
	MOVQ	DI, AX		// argc 
	MOVQ	SI, BX		// argv
        //说明：这里做一个扩栈操作，为了把寄存器中的argc和argv存储到当前栈中去
	SUBQ	$(4*8+7), SP		// 2args 2auto
	//说明：调整栈顶寄存器使其按16字节对齐 (^15=-16)
        ANDQ	$~15, SP
        //说明：把argc和argv入栈
	MOVQ	AX, 16(SP) 
	MOVQ	BX, 24(SP)

        //说明：主要是为g0分配64k的栈
	// create istack out of the given (operating system) stack.
	// _cgo_init may update stackguard.
	MOVQ	$runtime·g0(SB), DI // DI=g0
	LEAQ	(-64*1024+104)(SP), BX // BX=SP-64*1024+104
	MOVQ	BX, g_stackguard0(DI) // g0.g_stackguard0=SP-64*1024+104
	MOVQ	BX, g_stackguard1(DI) // g0.g_stackguard1=SP-64*1024+104
	MOVQ	BX, (g_stack+stack_lo)(DI) // g0.g_stack.stack_lo=SP-64*1024+104
	MOVQ	SP, (g_stack+stack_hi)(DI) // g0.g_stack.stack_hi=SP

        //说明：获取cpu信息，主要判断是不是intel平台，如果是就设置一个标志
	// find out information about the processor we're on
	MOVL	$0, AX
	CPUID 
	MOVL	AX, SI
	CMPL	AX, $0 
	JE	nocpuinfo //说明：没有获取到cpu信息，直接跳转到nocpuinfo

	// Figure out how to serialize RDTSC.
	// On Intel processors LFENCE is enough. AMD requires MFENCE.
	// Don't know about the rest, so let's do MFENCE.
	CMPL	BX, $0x756E6547  // "Genu"
	JNE	notintel
	CMPL	DX, $0x49656E69  // "ineI"
	JNE	notintel
	CMPL	CX, $0x6C65746E  // "ntel"
	JNE	notintel
	//说明：cpu平台是intel，设置intel标志
	MOVB	$1, runtime·isIntel(SB)
notintel:     //说明：不管是不是intel平台这里代码都会执行
        //说明： 设置cpu信息到runtime·processorVersionInfo(SB)中
	// Load EAX=1 cpuid flags
	MOVL	$1, AX
	CPUID
	MOVL	AX, runtime·processorVersionInfo(SB)

nocpuinfo:
	// if there is an _cgo_init, call it.
	MOVQ	_cgo_init(SB), AX
	TESTQ	AX, AX
	JZ	needtls //说明：先不考虑cgo开启的情况，这里跳转到needtls

needtls:
	LEAQ	runtime·m0+m_tls(SB), DI    //说明：DI=&m0.m_tls，m中的tls是一个6元素的uinptr
	CALL	runtime·settls(SB)          //说明：调用settls，目的是m0与主线程绑定

> runtime.settls() /usr/lib/go/src/runtime/sys_linux_amd64.s:634 (hits total:1) (PC: 0x45f740)
TEXT runtime·settls(SB),NOSPLIT,$32
#ifdef GOOS_android
	// Android stores the TLS offset in runtime·tls_g.
	SUBQ	runtime·tls_g(SB), DI
#else
        //说明：DI=&m0.m_tls+8 (DI=&m0.m_tls[1])
        //说明：让DI指向了m中tls中的第二个元素地址
	ADDQ	$8, DI	// ELF wants to use -8(FS) 
#endif
        //说明：设置线程的fs段地址为&m0.m_tls[1]，用于线程本地存储
	MOVQ	DI, SI //@@ SI=&m0.m_tls[1]
	MOVQ	$0x1002, DI	// ARCH_SET_FS
	MOVQ	$SYS_arch_prctl, AX
	SYSCALL
	CMPQ	AX, $0xfffffffffffff001
	JLS	2(PC)
	MOVL	$0xf1, 0xf1  // crash //@@ 设置失败就强制 crash程序
	RET

>runtime.rt0_go() /usr/lib/go/src/runtime/asm_amd64.s:262
needtls:
	LEAQ	runtime·m0+m_tls(SB), DI    
	CALL	runtime·settls(SB)    //返回继续执行

        get_tls(BX)    //BX=FS
        MOVQ	$0x123, g(BX) 
        //说明：以上2行代码汇编表示：mov qword ptr fs:[0xfffffff8], 0x123
        //说明：可得知 BX=FS，FS=&m0.tls[1], -8(FS) = &m0.tls， 主要目的就是 m0.tls[0]=0x123
    
        //说明：以下代码，然后通过判断m0.m_tls[0]中的值是否为0x123,以此来证明FS是否设置成功
	MOVQ	runtime·m0+m_tls(SB), AX
	CMPQ	AX, $0x123 ,
	JEQ 2(PC)
	CALL	runtime·abort(SB)

ok:
        //说明：让m0和g0产生绑定
	// set the per-goroutine and per-mach "registers"
	get_tls(BX)
	LEAQ	runtime·g0(SB), CX
        // m.tls[0] = g0
	MOVQ	CX, g(BX)
	LEAQ	runtime·m0(SB), AX

	// save m->g0 = g0
	MOVQ	CX, m_g0(AX)
	// save m0 to g0->m
	MOVQ	AX, g_m(CX)

```
栈关系图：
![img_2.jpg](img_2.jpg)

runtime/asm_amd64.s:203
```text
	CLD				// convention is D is always left cleared
	CALL	runtime·check(SB) //@@ 通过sizeof检测各数据类型的长度，检测编译器是否有误

	MOVL	16(SP), AX		// copy argc
	MOVL	AX, 0(SP)
	MOVQ	24(SP), AX		// copy argv
	MOVQ	AX, 8(SP)
	//@@ 赋值全局 argc 和 argv
	//@@ 通过/proc/self/auxv 获取内存物理页大小 physPageSize
	CALL	runtime·args(SB)
	//@@ 获取cpu数量赋值ncpu
	//@@ 通过/sys/kernel/mm/transparent_hugepage/hpage_pmd_size获取physHugePageSize
	CALL	runtime·osinit(SB)
	CALL	runtime·schedinit(SB) //@@ 调度初始化
```
runtime/proc.go:654
```go
func schedinit() {
// raceinit must be the first call to race detector.
// In particular, it must be done before mallocinit below calls racemapshadow.
   
    //getg函数在源代码中没有对应的定义，由编译器插入类似下面两行代码
    //get_tls(CX) 
    //MOVQ g(CX), BX; BX存器里面现在放的是当前g结构体对象的地址
    _g_ := getg() // _g_ = &g0

    ......

    //设置最多启动10000个操作系统线程，也是最多10000个M
    sched.maxmcount = 10000

    ......
   
    mcommoninit(_g_.m) //初始化m0，因为从前面的代码我们知道g0->m = &m0

    ......

    sched.lastpoll = uint64(nanotime())
    procs := ncpu  //系统中有多少核，就创建和初始化多少个p结构体对象
    if n, ok := atoi32(gogetenv("GOMAXPROCS")); ok && n > 0 {
        procs = n //如果环境变量指定了GOMAXPROCS，则创建指定数量的p
    }
    if procresize(procs) != nil {//创建和初始化全局变量allp
        throw("unknown runnable goroutine during bootstrap")
    }

    ......
}
```

runtime/proc.go:781
```go
func mcommoninit(mp *m) {
    _g_ := getg() //初始化过程中_g_ = g0

    // g0 stack won't make sense for user (and is not necessary unwindable).
    if _g_ != _g_.m.g0 {  //函数调用栈traceback，不需要关心
        callers(1, mp.createstack[:])
    }

    lock(&sched.lock)
    if sched.mnext+1 < sched.mnext {
        throw("runtime: thread ID overflow")
    }
    mp.id = sched.mnext
    sched.mnext++
    checkmcount() //检查已创建系统线程是否超过了数量限制（10000）

    //random初始化
    mp.fastrand[0] = 1597334677 * uint32(mp.id)
    mp.fastrand[1] = uint32(cputicks())
    if mp.fastrand[0]|mp.fastrand[1] == 0 {
        mp.fastrand[1] = 1
    }

    //创建用于信号处理的gsignal，只是简单的从堆上分配一个g结构体对象,然后把栈设置好就返回了
    mpreinit(mp)
    if mp.gsignal != nil {
        mp.gsignal.stackguard1 = mp.gsignal.stack.lo + _StackGuard
    }

    //把m挂入全局链表allm之中
    // Add to allm so garbage collector doesn't free g->m
    // when it is just in a register or thread-local storage.
    mp.alllink = allm 

    // NumCgoCall() iterates over allm w/o schedlock,
    // so we need to publish it safely.
    atomicstorep(unsafe.Pointer(&allm), unsafe.Pointer(mp))
    unlock(&sched.lock)

    // Allocate memory to hold a cgo traceback if the cgo call crashes.
    if iscgo || GOOS == "solaris" || GOOS == "windows" {
        mp.cgoCallers = new(cgoCallers)
    }
}
```

runtime/proc.go:4994
```go
func procresize(nprocs int32) *p {
    old := gomaxprocs //系统初始化时 gomaxprocs = 0

    ......

    // Grow allp if necessary.
    if nprocs > int32(len(allp)) { //初始化时 len(allp) == 0
        // Synchronize with retake, which could be running
        // concurrently since it doesn't run on a P.
        lock(&allpLock)
        if nprocs <= int32(cap(allp)) {
            allp = allp[:nprocs]
        } else { //初始化时进入此分支，创建allp 切片
            nallp := make([]*p, nprocs)
            // Copy everything up to allp's cap so we
            // never lose old allocated Ps.
            copy(nallp, allp[:cap(allp)])
            allp = nallp
        }
        unlock(&allpLock)
    }

    // initialize new P's
    //循环创建nprocs个p并完成基本初始化
    for i := int32(0); i < nprocs; i++ {
        pp := allp[i]
        if pp == nil {
            pp = new(p)//调用内存分配器从堆上分配一个struct p
            pp.id = i
            pp.status = _Pgcstop
            ......
            atomicstorep(unsafe.Pointer(&allp[i]), unsafe.Pointer(pp))
        }

        ......
    }

    ......

    _g_ := getg()  // _g_ = g0
    if _g_.m.p != 0 && _g_.m.p.ptr().id < nprocs {//初始化时m0->p还未初始化，所以不会执行这个分支
        // continue to use the current P
        _g_.m.p.ptr().status = _Prunning
        _g_.m.p.ptr().mcache.prepareForSweep()
    } else {//初始化时执行这个分支
        // release the current P and acquire allp[0]
        if _g_.m.p != 0 {//初始化时这里不执行
            _g_.m.p.ptr().m = 0
        }
        _g_.m.p = 0
        _g_.m.mcache = nil
        p := allp[0]
        p.m = 0
        p.status = _Pidle
        acquirep(p) //把p和m0关联起来，其实是这两个strct的成员相互赋值
        if trace.enabled {
            traceGoStart()
        }
    }
   
    //下面这个for 循环把所有空闲的p放入空闲链表
    var runnablePs *p
    for i := nprocs - 1; i >= 0; i-- {
        p := allp[i]
        if _g_.m.p.ptr() == p {//allp[0]跟m0关联了，所以是不能放任
            continue
        }
        p.status = _Pidle
        if runqempty(p) {//初始化时除了allp[0]其它p全部执行这个分支，放入空闲链表
            pidleput(p)
        } else {
            ......
        }
    }

    ......
   
    return runnablePs
}
/**
1.使用make([]*p, nprocs)初始化全局变量allp，即allp = make([]*p, nprocs)
2.循环创建并初始化nprocs个p结构体对象并依次保存在allp切片之中
3.把m0和allp[0]绑定在一起，即m0.p = allp[0], allp[0].m = m0
4.把除了allp[0]之外的所有p放入到全局变量sched的pidle空闲队列之中
*/
```

栈关系图：
![img_3.jpg](img_3.jpg)

### 3.构建runtime.main

runtime/asm_amd64.s:215
```text
// create a new goroutine to start program
MOVQ	$runtime·mainPC(SB), AX		// entry //@@ runtime·mainPC=runtime.main
PUSHQ	AX //@@ 把runtime.main 作为第二个参数压栈
PUSHQ	$0			// arg size  //@@ 第二个参数压栈 表示runtime.main需要的参数的大小
CALL	runtime·newproc(SB) //@@ 创建main goroutine
POPQ	AX
POPQ	AX

// start this M
CALL	runtime·mstart(SB) //@@ 主线程进入调度循环，运行刚刚创建的goroutine

//@@ 上面的mstart永远不应该返回的，如果返回了，一定是代码逻辑有问题，直接abort
CALL	runtime·abort(SB)	// mstart should never return
RET

// Prevent dead-code elimination of debugCallV2, which is
// intended to be called by debuggers.
MOVQ	$runtime·debugCallV2<ABIInternal>(SB), AX
RET

// mainPC is a function value for runtime.main, to be passed to newproc.
// The reference to runtime.main is made via ABIInternal, since the
// actual function (not the ABI0 wrapper) is needed by newproc.
DATA	runtime·mainPC+0(SB)/8,$runtime·main<ABIInternal>(SB)
GLOBL	runtime·mainPC(SB),RODATA,$8
```

runtime/proc.go:4250
```go
type funcval struct {
    fn uintptr
    // variable-size, fn-specific data here
}
// Create a new g running fn with siz bytes of arguments.
// Put it on the queue of g's waiting to run.
// The compiler turns a go statement into a call to this.
// Cannot split the stack because it assumes that the arguments
// are available sequentially after &fn; they would not be
// copied if a stack split occurred.
//go:nosplit
func newproc(siz int32, fn *funcval) {
    //函数调用参数入栈顺序是从右向左，而且栈是从高地址向低地址增长的
    //注意：argp指向fn函数的第一个参数，而不是newproc函数的参数
    //参数fn在栈上的地址+8的位置存放的是fn函数的第一个参数
    argp := add(unsafe.Pointer(&fn), sys.PtrSize)
    gp := getg()  //获取正在运行的g，初始化时是m0.g0
   
    //getcallerpc()返回一个地址，也就是调用newproc时由call指令压栈的函数返回地址，
    //对于我们现在这个场景来说，pc就是CALLruntime·newproc(SB)指令后面的POPQ AX这条指令的地址
    pc := getcallerpc()
   
    //systemstack的作用是切换到g0栈执行作为参数的函数
    //我们这个场景现在本身就在g0栈，因此什么也不做，直接调用作为参数的函数
    systemstack(func() {
        newproc1(fn, (*uint8)(argp), siz, gp, pc)
    })
}
```

runtime/proc.go:4275
```go
// Create a new g running fn with narg bytes of arguments starting
// at argp. callerpc is the address of the go statement that created
// this. The new g is put on the queue of g's waiting to run.
func newproc1(fn *funcval, argp *uint8, narg int32, callergp *g, callerpc uintptr) {
    //因为已经切换到g0栈，所以无论什么场景都有 _g_ = g0，当然这个g0是指当前工作线程的g0
    //对于我们这个场景来说，当前工作线程是主线程，所以这里的g0 = m0.g0
    _g_ := getg() 

    ......

    _p_ := _g_.m.p.ptr() //初始化时_p_ = g0.m.p，从前面的分析可以知道其实就是allp[0]
    newg := gfget(_p_) //从p的本地缓冲里获取一个没有使用的g，初始化时没有，返回nil
    if newg == nil {
         //new一个g结构体对象，然后从堆上为其分配栈，并设置g的stack成员和两个stackgard成员
        newg = malg(_StackMin)
        casgstatus(newg, _Gidle, _Gdead) //初始化g的状态为_Gdead
         //放入全局变量allgs切片中
        allgadd(newg) // publishes with a g->status of Gdead so GC scanner doesn't look at uninitialized stack.
    }
   
    ......
   
    //调整g的栈顶置针，无需关注
    totalSize := 4*sys.RegSize + uintptr(siz) + sys.MinFrameSize // extra space in case of reads slightly beyond frame
    totalSize += -totalSize & (sys.SpAlign - 1)                  // align to spAlign
    sp := newg.stack.hi - totalSize
    spArg := sp

    //......
   
    if narg > 0 {
         //把参数从执行newproc函数的栈（初始化时是g0栈）拷贝到新g的栈
        memmove(unsafe.Pointer(spArg), unsafe.Pointer(argp), uintptr(narg))
        // ......
    }
/**
1.从堆上分配一个g结构体对象
2.并为这个newg分配一个大小为2048字节的栈，并设置好newg的stack成员
3.把newg需要执行的函数的参数从执行newproc函数的栈（初始化时是g0栈）拷贝到newg的栈
 */
```

runtime/proc.go:4346
```go
    //把newg.sched结构体成员的所有成员设置为0
    memclrNoHeapPointers(unsafe.Pointer(&newg.sched), unsafe.Sizeof(newg.sched))
   
    //设置newg的sched成员，调度器需要依靠这些字段才能把goroutine调度到CPU上运行。
    newg.sched.sp = sp  //newg的栈顶
    newg.stktopsp = sp
    //newg.sched.pc表示当newg被调度起来运行时从这个地址开始执行指令
    //把pc设置成了goexit这个函数偏移1（sys.PCQuantum等于1）的位置，
    //至于为什么要这么做需要等到分析完gostartcallfn函数才知道
    newg.sched.pc = funcPC(goexit) + sys.PCQuantum // +PCQuantum so that previous instruction is in same function
    newg.sched.g = guintptr(unsafe.Pointer(newg))

    gostartcallfn(&newg.sched, fn) //调整sched成员和newg的栈
    
/**
newg.sched: 包含了调度器代码在调度goroutine到CPU运行时所必须的一些信息
newg.sched.sp: newg被调度起来运行时应该使用的栈的栈顶
newg.sche.pc: 当newg被调度起来运行时从这个地址开始执行指令

new.sched.pc 被设置成了goexit函数的第二条指令的地址而不是fn.fn, why?
 */
```

runtime/proc.go:4351
```go
// adjust Gobuf as if it executed a call to fn
// and then did an immediate gosave.
func gostartcallfn(gobuf *gobuf, fv *funcval) {
    var fn unsafe.Pointer
    if fv != nil {
        fn = unsafe.Pointer(fv.fn) //fn: gorotine的入口地址，初始化时对应的是runtime.main
    } else {
        fn = unsafe.Pointer(funcPC(nilfunc))
    }
    gostartcall(gobuf, fn, unsafe.Pointer(fv))
}
/**
gostartcallfn首先从参数fv中提取出函数地址（初始化时是runtime.main）
然后继续调用gostartcall函数
 */
```

runtime/stack.go:1119
```go
// adjust Gobuf as if it executed a call to fn with context ctxt
// and then did an immediate gosave.
func gostartcall(buf *gobuf, fn, ctxt unsafe.Pointer) {
    sp := buf.sp //newg的栈顶，目前newg栈上只有fn函数的参数，sp指向的是fn的第一参数
    if sys.RegSize > sys.PtrSize {
        sp -= sys.PtrSize
        *(*uintptr)(unsafe.Pointer(sp)) = 0
    }
    sp -= sys.PtrSize //为返回地址预留空间，
    //这里在伪装fn是被goexit函数调用的，使得fn执行完后返回到goexit继续执行，从而完成清理工作
    *(*uintptr)(unsafe.Pointer(sp)) = buf.pc //在栈上放入goexit+1的地址
    buf.sp = sp //重新设置newg的栈顶寄存器
    //这里才真正让newg的ip寄存器指向fn函数，注意，这里只是在设置newg的一些信息，newg还未执行，
    //等到newg被调度起来运行时，调度器会把buf.pc放入cpu的IP寄存器，
    //从而使newg得以在cpu上真正的运行起来
    buf.pc = uintptr(fn) 
    buf.ctxt = ctxt
}
/**
1.调整newg的栈空间，把goexit函数的第二条指令的地址入栈，伪造成goexit函数调用了fn，从而使fn执行完成后执行ret指令时返回到goexit继续执行完成最后的清理工作
2.重新设置newg.buf.pc 为需要执行的函数的地址，即fn，我们这个场景为runtime.main函数的地址
 */
```

runtime/proc.go:4366
```go
    //设置g的状态为_Grunnable，表示这个g代表的goroutine可以运行了
    casgstatus(newg, _Gdead, _Grunnable)

    ......
   
    //把newg放入_p_的运行队列，初始化的时候一定是p的本地运行队列，其它时候可能因为本地队列满了而放入全局队列
    runqput(_p_, newg, true)

    ......
}
```
runtime/proc.go:5953
```go
//优先把gp放入p的runnext,假如p的runnext有值会把旧的runnext放入到runq
//runq满了就会放全局队列
func runqput(_p_ *p, gp *g, next bool) {
	//初始化时 next=true 不执行
	if randomizeScheduler && next && fastrand()%2 == 0 {
		next = false
	}

	//初始化时 next=true 执行
	if next {
	retryNext:
	    //先把旧runnext拿出来
		oldnext := _p_.runnext 
		//把newg写入到_p_.runnext中
		if !_p_.runnext.cas(oldnext, guintptr(unsafe.Pointer(gp))) {
			goto retryNext
		}
		if oldnext == 0 { //初始化时不存在旧的runnext，所以到这里就return
			return
		}
		//把旧的runnext赋值给gp
		// Kick the old runnext out to the regular run queue.
		gp = oldnext.ptr()
	}

retry:
	h := atomic.LoadAcq(&_p_.runqhead) // load-acquire, synchronize with consumers
	t := _p_.runqtail
	//runq没有满，就假如到runq的尾部
	if t-h < uint32(len(_p_.runq)) {
		_p_.runq[t%uint32(len(_p_.runq))].set(gp)
		atomic.StoreRel(&_p_.runqtail, t+1) // store-release, makes the item available for consumption
		return
	}
	//p的本地队列满了，把本地队列的一半+1 转移到全局队列
	if runqputslow(_p_, gp, h, t) {
		return
	}
	// the queue is not full, now the put above must succeed
	goto retry
}
```
栈关系图
![img_5.jpg](img_5.jpg)

```text
总结：
1.runtime.main 对应 newg结构体
2.初始化newg.sched
    newg.sched.sp 指向newg的栈顶
    newg.sched.pc 指向runtime.main函数的第一条指令
3.伪造了runtime.exit(SB)调用当前goroutine的假想，当前go执行完会直接执行runtime.exit1(SB)
4.newg已经放入与当前主线程绑定的p结构体对象的本地运行队列头部
5.newg的m成员为nil，因为它还没有被调度起来运行，也就没有跟任何m进行绑定
```

### 4.调度runtime.main
