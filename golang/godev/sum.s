#include "textflag.h"

// func sum(a, b int) int
TEXT ·sum(SB), NOSPLIT, $16-24
    MOVQ arg0+0(FP), AX
    MOVL arg0+0(FP), BX
    MOVQ AX, ret0+16(FP)
    RET

// func callAdd(a, b int) int
// call func add(a, b int) int
TEXT ·callAdd(SB), NOSPLIT,$0x18-0x18
    MOVQ $0x0, 0x38(SP)
    MOVQ arg0+0(FP), AX
    MOVQ arg1+8(FP), BX
    MOVQ AX, 0(SP)
    MOVQ BX, 0x8(SP)
    CALL ·add(SB)
    MOVQ 0x10(SP), AX
    MOVQ AX, 0x38(SP)
    RET
