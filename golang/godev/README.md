### 1. 构建docker开发环境
```shell
#!/bin/sh

echo "FROM centos" > Dockerfile

#yum change aliyun.repo 
#yum install
echo "RUN curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-8.repo && \
sed -i 's/enabled=0/enabled=1/g' /etc/yum.repos.d/CentOS-Base.repo && \
yum clean all && \
yum makecache && \
yum update -y && \
yum install -y binutils vim gdb" >> Dockerfile

#go1.17.2
echo "RUN curl -o '/tmp/go.tar.gz' 'https://dl.google.com/go/go1.17.2.linux-amd64.tar.gz' && \
tar -zxf /tmp/go.tar.gz -C /usr/lib/ && ln -s /usr/lib/go/bin/* /usr/local/bin/" >> Dockerfile

#dlv latest
echo "RUN go install github.com/go-delve/delve/cmd/dlv@latest && \
ln -s \$(go env GOPATH)/bin/dlv /usr/local/bin/dlv" >> Dockerfile

docker stop godev
docker rm godev
docker rmi godev

docker build -t godev .
v_path=$(go env GOPATH)
if [ -z "$(go env GOPATH)" ]
then
    v_path=~/go
fi
docker run -dit --name godev -v $v_path:/root/go godev bash
```

### 2.进入docker容器
```shell
docker exec -it godev bash
```

### 3.创建godev项目用于调试
```text
mkdir -p $(go env GOPATH)/src/godev
cd $(go env GOPATH)/src/godev
```

#### 3.1 创建 [sum.go](./sum.go)
```go
package main

import "fmt"

func sum(int, int) int

func callAdd(int, int) int

func add(a, b int) int {
	return a+b
}

func main() {
	fmt.Println("sum:", sum(1, 2))
	fmt.Println("add:", callAdd(2, 3))
}
```

#### 3.2 创建 [sum.s](./sum.s)
```text
#include "textflag.h"

// func sum(a, b int) int
TEXT ·sum(SB), NOSPLIT, $16-24
    MOVQ arg0+0(FP), AX
    ADDQ arg1+8(FP), AX
    MOVQ AX, ret0+16(FP)
    RET

// func callAdd(a, b int) int
// call func add(a, b int) int
TEXT ·callAdd(SB), NOSPLIT,$0x18-0x18
    MOVQ $0x0, 0x38(SP)
    MOVQ arg0+0(FP), AX
    MOVQ arg1+8(FP), BX
    MOVQ AX, 0(SP)
    MOVQ BX, 0x8(SP)
    CALL ·add(SB)
    MOVQ 0x10(SP), AX
    MOVQ AX, 0x38(SP)
    RET
    
//*-* 注意： 文件尾部的换行符是必须的
```

### 4.编译sum.go
```shell
go mod init
go mod tidy
go build -o sum -gcflags="-N -l" .
```

### 5.程序的入口
```text
#1.打印程序入口使用readelf -h 读取elf file header, 其中 Entry point address 对应的 0x45c200 就是程序的入口
[root@8fa49365f2d9 godev]# readelf -h ./sum
ELF Header:
  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00 
  Class:                             ELF64
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              EXEC (Executable file)
  Machine:                           Advanced Micro Devices X86-64
  Version:                           0x1
  Entry point address:               0x45c200
  Start of program headers:          64 (bytes into file)
  Start of section headers:          456 (bytes into file)
  Flags:                             0x0
  Size of this header:               64 (bytes)
  Size of program headers:           56 (bytes)
  Number of program headers:         7
  Size of section headers:           64 (bytes)
  Number of section headers:         23
  Section header string table index: 3
  
#2.gdb打印程序入口, 其中 Entry point 对应的 0x45c200 就是程序的入口
#2.1 使用 b 对入口地址 0x45c200 打断点
#2.2 使用 r 启动调试，程序会在 0x45c200 处停留
#2.3 使用 l 显示调试处的几行代码 (gdb调试这里就不重点说了，可自动百度哈)
[root@8fa49365f2d9 godev]# gdb ./sum
(gdb) info files
Symbols from "/root/go/src/gitee.com/it-wind/knowledge/course/golang/godev/sum".
Local exec file:
	`/root/go/src/gitee.com/it-wind/knowledge/course/golang/godev/sum', file type elf64-x86-64.
	Entry point: 0x45c200
	0x0000000000401000 - 0x000000000047e5e7 is .text
	0x000000000047f000 - 0x00000000004b3e8e is .rodata
	0x00000000004b4020 - 0x00000000004b44f8 is .typelink
	0x00000000004b4500 - 0x00000000004b4558 is .itablink
	0x00000000004b4558 - 0x00000000004b4558 is .gosymtab
	0x00000000004b4560 - 0x000000000050cf38 is .gopclntab
	0x000000000050d000 - 0x000000000050d020 is .go.buildinfo
	0x000000000050d020 - 0x000000000051d5e0 is .noptrdata
	0x000000000051d5e0 - 0x0000000000524dd0 is .data
	0x0000000000524de0 - 0x0000000000553d08 is .bss
	0x0000000000553d20 - 0x0000000000559080 is .noptrbss
	0x0000000000400f9c - 0x0000000000401000 is .note.go.buildid
(gdb) b *0x45c200
Breakpoint 1 at 0x45c200: file /usr/lib/go/src/runtime/rt0_linux_amd64.s, line 8.
(gdb) r
Starting program: /root/go/src/gitee.com/it-wind/knowledge/course/golang/godev/sum 
Breakpoint 1, _rt0_amd64_linux () at /usr/lib/go/src/runtime/rt0_linux_amd64.s:8
8		JMP	_rt0_amd64(SB)
(gdb) l
3	// license that can be found in the LICENSE file.
4	
5	#include "textflag.h"
6	
7	TEXT _rt0_amd64_linux(SB),NOSPLIT,$-8
8		JMP	_rt0_amd64(SB)
9	
10	TEXT _rt0_amd64_linux_lib(SB),NOSPLIT,$0
11		JMP	_rt0_amd64_lib(SB)

#3.使用dlv会自动找到程序的入口地址，可直接调试
[root@8fa49365f2d9 godev]# dlv exec ./sum
Type 'help' for list of commands.
(dlv) l
> _rt0_amd64_linux() /usr/lib/go/src/runtime/rt0_linux_amd64.s:8 (PC: 0x45c200)
Warning: debugging optimized function
     3:	// license that can be found in the LICENSE file.
     4:	
     5:	#include "textflag.h"
     6:	
     7:	TEXT _rt0_amd64_linux(SB),NOSPLIT,$-8
=>   8:		JMP	_rt0_amd64(SB)
     9:	
    10:	TEXT _rt0_amd64_linux_lib(SB),NOSPLIT,$0
    11:		JMP	_rt0_amd64_lib(SB)
```

### 6.dlv 简单参数说明
```text
简单参数说明
b 设置断点
    b runqput
    b *0x464840
clear 断点编号 删除断点
clearall 删除所有断点
bp 输出所有设置的断点
c 停留到下一个断点，若无断点会执行完程序
next 单步执行，不进入调用函数内部
s 单步执行，进入调用函数内部
si 单步调试 遇到jump指令请使用si命令步进
p 可以打印变量
bt 输出当前函数调用栈
l 输出源码
regs 输出所有寄存器的值
disass 输出反汇编代码
q 退出
```

### 7.dlv调试程序
```text
[root@8fa49365f2d9 godev]# dlv exec ./sum
Type 'help' for list of commands.
(dlv) l ###显示源码
> _rt0_amd64_linux() /usr/lib/go/src/runtime/rt0_linux_amd64.s:8 (PC: 0x45c200)
Warning: debugging optimized function
     3:	// license that can be found in the LICENSE file.
     4:	
     5:	#include "textflag.h"
     6:	
     7:	TEXT _rt0_amd64_linux(SB),NOSPLIT,$-8
=>   8:		JMP	_rt0_amd64(SB)
     9:	
    10:	TEXT _rt0_amd64_linux_lib(SB),NOSPLIT,$0
    11:		JMP	_rt0_amd64_lib(SB)
(dlv) si ###单核单步步进
> _rt0_amd64() /usr/lib/go/src/runtime/asm_amd64.s:16 (PC: 0x458840)
Warning: debugging optimized function
    11:	// _rt0_amd64 is common startup code for most amd64 systems when using
    12:	// internal linking. This is the entry point for the program from the
    13:	// kernel for an ordinary -buildmode=exe program. The stack holds the
    14:	// number of arguments and the C-style argv.
    15:	TEXT _rt0_amd64(SB),NOSPLIT,$-8
=>  16:		MOVQ	0(SP), DI	// argc
    17:		LEAQ	8(SP), SI	// argv
    18:		JMP	runtime·rt0_go(SB)
    19:	
    20:	// main is common startup code for most amd64 systems when using
    21:	// external linking. The C startup code will call the symbol "main"
(dlv) b rt0_go ###对rt0_go函数打断点
Breakpoint 1 set at 0x458860 for runtime.rt0_go() /usr/lib/go/src/runtime/asm_amd64.s:83
(dlv) c ###跳到rt0_go函数断点处
> runtime.rt0_go() /usr/lib/go/src/runtime/asm_amd64.s:83 (hits total:1) (PC: 0x458860)
Warning: debugging optimized function
    78:	DATA _rt0_amd64_lib_argv<>(SB)/8, $0
    79:	GLOBL _rt0_amd64_lib_argv<>(SB),NOPTR, $8
    80:	
    81:	TEXT runtime·rt0_go(SB),NOSPLIT|TOPFRAME,$0
    82:		// copy arguments forward on an even stack
=>  83:		MOVQ	DI, AX		// argc
    84:		MOVQ	SI, BX		// argv
    85:		SUBQ	$(4*8+7), SP		// 2args 2auto
    86:		ANDQ	$~15, SP
    87:		MOVQ	AX, 16(SP)
    88:		MOVQ	BX, 24(SP)
(dlv) b main.main ###对main函数打断点
Breakpoint 2 set at 0x47e30f for main.main() ./sum.go:13
(dlv) b main.sum
Breakpoint 3 set at 0x47e500 for main.sum() ./sum.s:6
(dlv) b add ###也可不指定包名,当出现多个重名函数会提示,这时请尽可能输入全函数的包名和函数名
Command failed: Location "add" ambiguous: reflect.add, internal/reflectlite.add, runtime.add, runtime.(*notInHeap).add, runtime.offAddr.add…
(dlv) b main.add ###这里可以看到main.add是编译器生成的<autogenerated>
Breakpoint 4 set at 0x47e5a0 for main.add() <autogenerated>:1
(dlv) funcs ^main. ###通过funcs可根据给定的字符串模糊匹配出函数(可以看到main.add有两个，1个系统生成，一个sum.go中实现的)
main.add
main.add
main.callAdd
main.main
main.sum
(dlv) b sum.go:9 ###通过源码文件:行号设置断点，这里对main.add设置断点
Breakpoint 5 set at 0x47e2c0 for main.add() ./sum.go:9
(dlv) b callAdd
Breakpoint 6 set at 0x47e540 for main.callAdd() ./sum.s:14
(dlv) bp ###输出所有已经设置的断点
Breakpoint runtime-fatal-throw (enabled) at 0x42f900 for runtime.throw() /usr/lib/go/src/runtime/panic.go:1188 (0)
Breakpoint unrecovered-panic (enabled) at 0x42fc60 for runtime.fatalpanic() /usr/lib/go/src/runtime/panic.go:1271 (0)
	print runtime.curg._panic.arg
Breakpoint 1 (enabled) at 0x458860 for runtime.rt0_go() /usr/lib/go/src/runtime/asm_amd64.s:83 (1)
Breakpoint 2 (enabled) at 0x47e30f for main.main() ./sum.go:13 (0)
Breakpoint 3 (enabled) at 0x47e500 for main.sum() ./sum.s:6 (0)
Breakpoint 4 (enabled) at 0x47e5a0 for main.add() <autogenerated>:1 (0)
Breakpoint 5 (enabled) at 0x47e2c0 for main.add() ./sum.go:9 (0)
Breakpoint 6 (enabled) at 0x47e540 for main.callAdd() ./sum.s:14 (0)
(dlv) clear 1 ### 删除断点rt0_go()
Breakpoint 1 cleared at 0x458860 for runtime.rt0_go() /usr/lib/go/src/runtime/asm_amd64.s:83
(dlv) c
> main.main() ./sum.go:13 (hits goroutine(1):1 total:1) (PC: 0x47e30f)
     8:	
     9:	func add(a, b int) int {
    10:		return a+b
    11:	}
    12:	
=>  13:	func main() {
    14:		fmt.Println("sum:", sum(1, 2))
    15:		fmt.Println("add:", callAdd(2, 3))
    16:	}
(dlv) bt ### 打印函数调用栈，由此可见main函数并不是程序的入口
0  0x000000000047e30f in main.main
   at ./sum.go:13
1  0x0000000000432047 in runtime.main
   at /usr/lib/go/src/runtime/proc.go:255
2  0x000000000045ab61 in runtime.goexit
   at /usr/lib/go/src/runtime/asm_amd64.s:1581
(dlv) c ### 跳转到main.sum断点处
> main.sum() ./sum.s:6 (hits goroutine(1):1 total:1) (PC: 0x47e500)
     1:	//*-* 注意： 文件尾部的换行符是必须的
     2:	
     3:	#include "textflag.h"
     4:	
     5:	// func sum(a, b int) int
=>   6:	TEXT ·sum(SB), NOSPLIT, $16-24
     7:	    MOVQ arg0+0(FP), AX
     8:	    ADDQ arg1+8(FP), AX
     9:	    MOVQ AX, ret0+16(FP)
    10:	    RET
    11:	
(dlv) s
> main.sum() ./sum.s:7 (PC: 0x47e50e)
     2:	
     3:	#include "textflag.h"
     4:	
     5:	// func sum(a, b int) int
     6:	TEXT ·sum(SB), NOSPLIT, $16-24
=>   7:	    MOVQ arg0+0(FP), AX
     8:	    ADDQ arg1+8(FP), AX
     9:	    MOVQ AX, ret0+16(FP)
    10:	    RET
    11:	
    12:	// func callAdd(a, b int) int
(dlv) regs ### 在AX被赋值前，可以看到AX里面是一些脏数据
    Rip = 0x000000000047e50e
    Rsp = 0x000000c00005eec0
    Rax = 0x000000000047e300
    Rbx = 0x0000000000000000
    Rcx = 0x0000000000000000
    Rdx = 0x000000000049c650
    Rsi = 0x0000000000000001
    Rdi = 0x000000000050fbb8
    Rbp = 0x000000c00005eed0
     R8 = 0x0000000000000001
     R9 = 0x0000000000000008
    R10 = 0x00007fe5cf9623b0
    R11 = 0x0000000000000000
    R12 = 0x000000c00005ef60
    R13 = 0x000000000049c438
    R14 = 0x000000c0000001a0
    R15 = 0x00007fe5a8e28811
 Rflags = 0x0000000000000206	[PF IF IOPL=0]
     Es = 0x0000000000000000
     Cs = 0x0000000000000033
     Ss = 0x000000000000002b
     Ds = 0x0000000000000000
     Fs = 0x0000000000000000
     Gs = 0x0000000000000000
Fs_base = 0x0000000000525870
Gs_base = 0x0000000000000000
(dlv) s
> main.sum() ./sum.s:8 (PC: 0x47e513)
     3:	#include "textflag.h"
     4:	
     5:	// func sum(a, b int) int
     6:	TEXT ·sum(SB), NOSPLIT, $16-24
     7:	    MOVQ arg0+0(FP), AX
=>   8:	    ADDQ arg1+8(FP), AX
     9:	    MOVQ AX, ret0+16(FP)
    10:	    RET
    11:	
    12:	// func callAdd(a, b int) int
    13:	// call func add(a, b int) int
(dlv) regs ### AX值等于0X1，正是sum函数的第一个入参
    Rip = 0x000000000047e513
    Rsp = 0x000000c00005eec0
    Rax = 0x0000000000000001
    Rbx = 0x0000000000000000
    Rcx = 0x0000000000000000
    Rdx = 0x000000000049c650
    Rsi = 0x0000000000000001
    Rdi = 0x000000000050fbb8
    Rbp = 0x000000c00005eed0
     R8 = 0x0000000000000001
     R9 = 0x0000000000000008
    R10 = 0x00007fe5cf9623b0
    R11 = 0x0000000000000000
    R12 = 0x000000c00005ef60
    R13 = 0x000000000049c438
    R14 = 0x000000c0000001a0
    R15 = 0x00007fe5a8e28811
 Rflags = 0x0000000000000206	[PF IF IOPL=0]
     Es = 0x0000000000000000
     Cs = 0x0000000000000033
     Ss = 0x000000000000002b
     Ds = 0x0000000000000000
     Fs = 0x0000000000000000
     Gs = 0x0000000000000000
Fs_base = 0x0000000000525870
Gs_base = 0x0000000000000000
(dlv) s
> main.sum() ./sum.s:9 (PC: 0x47e518)
     4:	
     5:	// func sum(a, b int) int
     6:	TEXT ·sum(SB), NOSPLIT, $16-24
     7:	    MOVQ arg0+0(FP), AX
     8:	    ADDQ arg1+8(FP), AX
=>   9:	    MOVQ AX, ret0+16(FP)
    10:	    RET
    11:	
    12:	// func callAdd(a, b int) int
    13:	// call func add(a, b int) int
    14:	TEXT ·callAdd(SB), NOSPLIT,$0x18-0x18
(dlv) regs ###执行完ADD后 AX=0x03
    Rip = 0x000000000047e518
    Rsp = 0x000000c00005eec0
    Rax = 0x0000000000000003
    Rbx = 0x0000000000000000
    Rcx = 0x0000000000000000
    Rdx = 0x000000000049c650
    Rsi = 0x0000000000000001
    Rdi = 0x000000000050fbb8
    Rbp = 0x000000c00005eed0
     R8 = 0x0000000000000001
     R9 = 0x0000000000000008
    R10 = 0x00007fe5cf9623b0
    R11 = 0x0000000000000000
    R12 = 0x000000c00005ef60
    R13 = 0x000000000049c438
    R14 = 0x000000c0000001a0
    R15 = 0x00007fe5a8e28811
 Rflags = 0x0000000000000206	[PF IF IOPL=0]
     Es = 0x0000000000000000
     Cs = 0x0000000000000033
     Ss = 0x000000000000002b
     Ds = 0x0000000000000000
     Fs = 0x0000000000000000
     Gs = 0x0000000000000000
Fs_base = 0x0000000000525870
Gs_base = 0x0000000000000000
(dlv) c ###callAdd 就是调用了add不展开
sum: 3 ###输出sum: 3
> main.callAdd() ./sum.s:14 (hits goroutine(1):1 total:1) (PC: 0x47e540)
     9:	    MOVQ AX, ret0+16(FP)
    10:	    RET
    11:	
    12:	// func callAdd(a, b int) int
    13:	// call func add(a, b int) int
=>  14:	TEXT ·callAdd(SB), NOSPLIT,$0x18-0x18
    15:	    MOVQ $0x0, 0x38(SP)
    16:	    MOVQ arg0+0(FP), AX
    17:	    MOVQ arg1+8(FP), BX
    18:	    MOVQ AX, 0(SP)
    19:	    MOVQ BX, 0x8(SP)
(dlv) c ###这里main.add编译器生成
> main.add() <autogenerated>:1 (hits goroutine(1):1 total:1) (PC: 0x47e5a0)
(dlv) disass ### 反汇编可以看到把参数从父函数的栈桢上复制到寄存器中调用main.add
TEXT main.add(SB) <autogenerated>
=>	<autogenerated>:1	0x47e5a0*	4883ec20		sub rsp, 0x20
	<autogenerated>:1	0x47e5a4	48896c2418		mov qword ptr [rsp+0x18], rbp
	<autogenerated>:1	0x47e5a9	488d6c2418		lea rbp, ptr [rsp+0x18]
	<autogenerated>:1	0x47e5ae	48c744243800000000	mov qword ptr [rsp+0x38], 0x0
	<autogenerated>:1	0x47e5b7	488b5c2430		mov rbx, qword ptr [rsp+0x30]
	.:0			0x47e5bc	488b442428		mov rax, qword ptr [rsp+0x28]
	.:0			0x47e5c1	450f57ff		xorps xmm15, xmm15
	.:0			0x47e5c5	644c8b3425f8ffffff	mov r14, qword ptr fs:[0xfffffff8]
	.:0			0x47e5ce	e8edfcffff		call $main.add
	.:0			0x47e5d3	4889442410		mov qword ptr [rsp+0x10], rax
	.:0			0x47e5d8	4889442438		mov qword ptr [rsp+0x38], rax
	.:0			0x47e5dd	488b6c2418		mov rbp, qword ptr [rsp+0x18]
	.:0			0x47e5e2	4883c420		add rsp, 0x20
	.:0			0x47e5e6	c3			ret
(dlv) 
(dlv) c ###断点停留在main.add
> main.add() ./sum.go:9 (hits goroutine(1):1 total:1) (PC: 0x47e2c0)
     4:	
     5:	func sum(int, int) int
     6:	
     7:	func callAdd(int, int) int
     8:	
=>   9:	func add(a, b int) int {
    10:		return a+b
    11:	}
    12:	
    13:	func main() {
    14:		fmt.Println("sum:", sum(1, 2))
(dlv) 
(dlv) p a ###打印变量a
2
(dlv) p b ###打印变量b
3
(dlv) disass ###反汇编main.add
TEXT main.add(SB) /root/go/src/godev/sum.go
=>	sum.go:9	0x47e2c0*	4883ec10		sub rsp, 0x10
	sum.go:9	0x47e2c4	48896c2408		mov qword ptr [rsp+0x8], rbp
	sum.go:9	0x47e2c9	488d6c2408		lea rbp, ptr [rsp+0x8]
	sum.go:9	0x47e2ce	4889442418		mov qword ptr [rsp+0x18], rax
	sum.go:9	0x47e2d3	48895c2420		mov qword ptr [rsp+0x20], rbx
	sum.go:9	0x47e2d8	48c7042400000000	mov qword ptr [rsp], 0x0
	sum.go:10	0x47e2e0	488b442418		mov rax, qword ptr [rsp+0x18]
	sum.go:10	0x47e2e5	4803442420		add rax, qword ptr [rsp+0x20]
	.:0		0x47e2ea	48890424		mov qword ptr [rsp], rax
	.:0		0x47e2ee	488b6c2408		mov rbp, qword ptr [rsp+0x8]
	.:0		0x47e2f3	4883c410		add rsp, 0x10
	.:0		0x47e2f7	c3			ret
(dlv) c ###程序运行结束
add: 5 ###输出add: 5
```