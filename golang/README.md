<h2> 课程大纲 </h2>

```text
作者：清风
微信：GuiQiangVX
订阅号微信：it-wind
订阅号：曰码
```

### 第一节 [汇编基础](./asm-foundation/README.md)
```text
1.寄存器
2.内存
3.函数栈
4.汇编指令
5.函数调用
6.系统线程简述
7.TLS线程本地存储
```

### 第二节 [引导阶段](./scheduler-init/README.md)
调试环境：[构建基于docker开发环境](./godev/README.md)
```text
8.程序启动和调度器初始化
9.创建main
10.启动main
11.调度流程
```

### 第三节 [调度阶段](./scheduler-strategy/README.md)
```text
12.被动调度
13.主动调度
14.抢占调度
```