### 1.编译hello.go
* go build -x hello.go
* 拆解成如下两步
```shell
go tool compile
go tool link
```

### 2.编译过程
1.编译：
* 编译前端
```text
词法分析：go代码转换为token流
语法分析：token流转换为抽象语法树AST
语义分析：在抽象语法树AST上做类型检查
```
* 编译后端
```text
中间代码生成：复杂表达式拆解成多个简单表达式(Single)、静态单赋值(Static)
中间代码优化
机器代码生成：CPU可以理解的指令
```

2.链接：
```text
编译后的指令地址都是基于段偏移的
```
* 链接过程：
```text
1.依赖库和主函数打包到一起
2.给所有指令的偏移地址重定位为全局唯一的虚拟地址
```

### 3.编译、反编译工具
1.编译
* go tool compile -S hello.go | grep hello.go:5
```text
生成hello.o目标文件
把目标的汇编内容输出
```

hello.go
```go
package main

func main()  {
    var a = "hello"
    var b = []byte(a)
    
    println(b)
}
```

hello.o | grep hello.go:5
```text
tt@TTdeMacBook-Pro godev % go tool compile -S hello.go| grep hello.go:5
        0x0014 00020 (hello.go:5)       LEAQ    ""..autotmp_2+40(SP), AX
        0x0019 00025 (hello.go:5)       LEAQ    go.string."hello"(SB), BX
        0x0020 00032 (hello.go:5)       MOVL    $5, CX
        0x0025 00037 (hello.go:5)       PCDATA  $1, $0
        0x0025 00037 (hello.go:5)       CALL    runtime.stringtoslicebyte(SB)
```
可以通过汇编代码看到string转byte是通过调用runtime.stringtoslicebyte函数完成的

2.反编译
hello.go
```go
package main

func main() {
	b := make(map[int]string, 300)
	d := make(chan string, 200)

	_, _ = b, d
}
```
* 对二进制可执行文件的反编译
* go build -o hello hello.go
* go tool objdump hello | grep -e hello.go:4 -e hello.go:5
```text
tt@TTdeMacBook-Pro godev % go tool objdump hello | grep -e hello.go:4 -e hello.go:5
  hello.go:4            0x1054bf4               488d4c2418              LEAQ 0x18(SP), CX                       
  hello.go:4            0x1054bf9               440f1139                MOVUPS X15, 0(CX)                       
  hello.go:4            0x1054bfd               488d542428              LEAQ 0x28(SP), DX                       
  hello.go:4            0x1054c02               440f113a                MOVUPS X15, 0(DX)                       
  hello.go:4            0x1054c06               488d542438              LEAQ 0x38(SP), DX                       
  hello.go:4            0x1054c0b               440f113a                MOVUPS X15, 0(DX)                       
  hello.go:4            0x1054c0f               488d056a5c0000          LEAQ runtime.types+23360(SB), AX        
  hello.go:4            0x1054c16               bb2c010000              MOVL $0x12c, BX                         
  hello.go:4            0x1054c1b               0f1f440000              NOPL 0(AX)(AX*1)                        
  hello.go:4            0x1054c20               e87b69fbff              CALL runtime.makemap(SB)                
  hello.go:5            0x1054c25               488d05343f0000          LEAQ runtime.types+15904(SB), AX        
  hello.go:5            0x1054c2c               bbc8000000              MOVL $0xc8, BX                          
  hello.go:5            0x1054c31               e88ae8faff              CALL runtime.makechan(SB)  
```
可以看到 make(map[int]string, 300) 和 make(chan string, 200) 分别调用 runtime.makemap(SB) 和 runtime.makechan(SB)函数实现

### 4.调试工具 [调试工具详细说明](../godev/README.md)