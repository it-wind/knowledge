### 1.寄存器
```text
寄存器是CPU内部的高速存储部件，用于暂存从内存读取而来的数据（包括指令）和CPU运算的中间结果
```
![img_1.png](img_1.png)

```text
寄存器分类：
4个数据寄存器(RAX、RBX、RCX和RDX)
    可拆分独立存取：
        4个数据寄存器低32位分别命名为：RAX、RBX、RCX和RDX
        4个数据寄存器低16位分别命名为：AX、BX、CX和DX
        4个低16位数据寄存器又可分割成8个独立的8位寄存器(AX：AH-AL、BX：BH-BL、CX：CH-CL、DX：DH-DL)
    特殊用途：
        寄存器RAX 累加和结果寄存器
        寄存器RBX 数据指针寄存器
        寄存器RCX 循环计数器
        寄存器RDX i/o指针
8个数据寄存器(R8~R15)
    64-bit模式下引入的通用寄存器
2个变址和指针寄存器(RSI和RDI) 
    RSI 源地址寄存器
    RDI 目的地址寄存器
2个指针寄存器(RBP和RSP) 
    RBP为基指针(Base Pointer)寄存器，用它可直接存取堆栈中的数据
    RSP为堆栈指针(Stack Pointer)寄存器，用它只可访问栈顶
6个段寄存器(ES、CS、SS、DS、FS和GS)
    CS            代码段寄存器
    DS、ES、FS、GS 数据段寄存器
    SS            堆栈段寄存器 
1个指令指针寄存器(RIP)
    指向下一条指令的地址
1个标志寄存器(RFlags)
    CF (bit 0)  进位标识，算术操作进行了进位和借位，则此位被设置
    PF (bit 2)  奇偶标识，结果包含奇数个1，则设置此位
    AF (bit 4)  辅助进位标识，结果的第3位像第4位借位，则此位被设置
    ZF (bit 6)  零标识，结果为零，此位设置
    SF (bit 7)  符号标识，若为负数则设置此位
    OF (bit 11) 溢出标识，结果像最高位符号位进行借位或者进位，此标志被设置
    TF (bit 8)  陷阱标识，设置进程可以被单步调试
    IF (bit 9)  中断标识，设置能够响应中断请求
    DF (bit 10) 方向标识，用于标示字符处理过程中指针移动方向
```

### 2.内存
* 内存的描述
```text
内存是计算机系统的存储设备，其主要作用是协助CPU在执行程序时存储数据和指令
*内存单元大小为1个字节
*任何大于一个字节的变量都存储在相邻的几个内存单元中
*访问任意内存地址时间成本是一样的
```

* 大小端存储模式：
```text
1.大端存储模式：数据的高字节保存在内存的低地址中，低字节保存在内存的高地址中
2.小端存储模式：数据的高字节保存在内存的高地址中，低字节保存在内存的低地址中
```
![img_27.png](img_27.png)

* 多线程进程的地址空间

![img_26.png](img_26.png)

```text
栈区：记录函数调用过程相关信息维护，包含：函数返回地址、局部变量、函数返回值、函数入参、寄存器临时数据存储等
mmap区：可将文件直接映射到内存，一般用于装载动态共享库、子线程栈区等
堆区：程序运行时动态分配的内存都位于堆中，这部分内存由内存分配器负责管理。该区域的大小会随着程序的运行而变化
bss段：通常用于存放未初始化的全局变量和静态局部变量、初始化值为0的全局变量及静态局部变量
数据段：存放程序中已初始化且不为0的全局变量及静态局部变量，程序加载完毕后数据区的大小也不会发生改变
代码段：包括能被CPU执行的机器代码（指令）和只读数据比如字符串常量，程序一旦加载完成代码区的大小就不会再变化
```

[内存分段地址转换](../addr-translation/README.md)

### 3.Plan9汇编指令
Plan9汇编和AMD64汇编寄存器的对比
![img_11.png](img_11.png)

```text
plan9虚拟寄存器
FP：使用形如 symbol+offset(FP) 的方式，引用函数的输入参数。例如 arg0+0(FP)
PC：在 x86 平台下对应 ip 寄存器，amd64 上则是 rip
SB：保存程序地址空间的起始地址，主要用来定位全局符号。go汇编中的函数定义、函数调用、全局变量定义以及对其引用会用到这个SB虚拟寄存器
SP：当前栈帧的局部变量的开始位置，假如局部变量都是 8 字节，第一个局部变量就可以用 localvar0-8(SP) 来表示
*如果是 symbol+offset(SP) 形式，则表示伪寄存器 SP。如果是 offset(SP) 则表示硬件寄存器 SP
*如果是通过反编译的汇编代码中的SP都是硬件寄存器 SP

汇编指令格式
操作码  [操作数]
操作码：操作码指示CPU执行什么操作，比如是执行加法，减法还是读写内存。每条指令都必须要有操作码
操作数：操作数是操作的对象，比如加法操作需要两个加数，这两个加数就是这条指令的操作数。操作数的个数一般是0个，1个或2个

数据传送
MOVB $1, DI     //1byte
MOVW $0x20, SI  //2bytes
MOVL $-7, CX    //4bytes
MOVQ $10, DX    //8bytes

计算
ADDQ  AX, BX   // BX += AX , 不同长度计算 ADDQ/ADDL/ADDW/ADDB
SUBQ  AX, BX   // BX -= AX , 不同长度计算 SUBQ/SUBL/SUBW/SUBB
IMULQ AX, BX   // BX *= AX , 不同长度计算 IMULQ/IMULL/IMULW

跳转
跳转指令 目标地址/标号/相对地址

无条件跳转指令
JMP $0x4008000  //跳转到 地址0x4008000处
JMP label       //跳转到 标号处，编译后实际上也是一个地址
JMP -2(PC)      //跳转到 当前指令前2行地址处
JMP 2(PC)       //跳转到 当前指令后2行地址处

有条件跳转
JZ          //结果为0跳转, ZF=1
JNZ         //结果不为0跳转, ZF=0
JE          //结果相等跳转, ZF=1
JNE         //结果不想等跳转, ZF=0

JB          //小于则跳转 (无符号数), CF=1
JBE         //小于等于则跳转(无符号数), CF=1 || ZF=1
JA          //大于则跳转 (无符号数), CF=0 && ZF=0
JAE         //大于等于则跳转 (无符号数), CF=0

JLS         //小于则跳转 (有符号数), SF!=OF
JLE         //小于等于则跳转 (有符号数), ZF=1 || SF!=OF
JG          //大于则跳转 (有符号数), ZF=0 && SF=OF
JGE         /大于等于则跳转 (有符号数), SF=OF

栈操作指令
SUBQ $0x20, SP //扩栈32字节
ADDQ $0x18, SP //缩栈24字节

PUSHQ/PUSHL AX //把AX中的数据压栈
POPQ/POPL BX   //把栈顶的数据弹出到BX中

函数相关指令
CALL $0x4002020 //调用函数
RET             //返回上层函数

寻址方式：
直接寻址
MOVQ    $0x10, SP  // SP=0x10

间接寻址
MOVQ    $0x10, 0(SP)        // *(SP+0)=0x10
MOVQ    $0x01, +8(SP)       // *(SP+8)=0x01
MOVQ    $0x02, -0x10(SP)    // *(SP-0x10)=0x02

指令拆解
PUSHQ AX 拆解为：
SUBQ $0x08, SP
MOVQ AX, 0(SP)

POPQ BX 拆解为：
MOVQ 0(SP), BX
ADDQ $0x08, SP

CALL $0x4009000 拆解为：
PUSH PC
JMP $0x4009000

RET 拆解为：
POP BX
JMP BX

变量的声明
DATA package_name·symbol+offset(SB)/width, value
例如：
DATA runtime·tls_g+0(SB)/8, $16
解释：
DATA 声明变量
package_name 包名
symbol 变量名
offset 相对于symbol的偏移(定义数组或字符串会使用到)
width 变量的大小
value 变量的取值

声明全局变量
DATA package_name·symbol+offset(SB)/width, value
GLOBL package_name·symbol+offset(SB), flags, $width
例如：
DATA runtime·tls_g+0(SB)/8, $16
GLOBL runtime·tls_g+0(SB), NOPTR, $8
解释：
GLOBL 声明变量为global
flags 下方统一说明
width 变量的大小

例如：定义一个全局的字符串
DATA phone+0(SB)/4, $"+110"
DATA phone+3(SB)/8, $"91110110"
GLOBL phone(SB), RODATA, $12

函数的声明
TEXT package_name·func_name(SB),flags,$width-width
例如：
TEXT runtime·gogo(SB), NOSPLIT, $16-8
解释：
TEXT 声明函数
runtime 包名
gogo 函数名
$16-8 函数的栈帧大小为16字节, 函数的参数和返回值共8字节

flags 枚举如下 (函数声明flags需要引入"textflag.h"头文件)：
DUPOK = 2
    It is legal to have multiple instances of this symbol in a single binary. 
    The linker will choose one of the duplicates to use.
NOSPLIT = 4
    (For TEXT items.) Don't insert the preamble to check if the stack must be split. 
    The frame for the routine, plus anything it calls, must fit in the spare space at the top of the stack segment. 
    Used to protect routines such as the stack splitting code itself.
RODATA = 8
    (For DATA and GLOBL items.) Put this data in a read-only section.
    NOPTR = 16
    (For DATA and GLOBL items.) This data contains no pointers and therefore does not need to be scanned by the garbage collector.
WRAPPER = 32
    (For TEXT items.) This is a wrapper function and should not count as disabling recover.
NEEDCTXT = 64
    (For TEXT items.) This function is a closure so it uses its incoming context register.
```

### 4.函数调用

假设 A()->B()->C()

![img_4.png](img_4.png)
```
栈的内容：
保存函数的局部变量；
向被调用函数传递参数；
返回函数的返回值；
保存函数的返回地址

操作栈的辅助寄存器：
rsp寄存器，始终指向函数调用栈栈顶
rbp寄存器，一般用来指向函数栈帧的起始位置
```
go语言中的函数调用过程
```go
package main

func sum(a, b int) int {
	a2 := a * a
	b2 := b * b
	c := a2 + b2

	return c
}

func main() {
	sum(1, 2)
}
```

go函数调用反汇编 main.main
```text
  0x000000000044f4e0 <+0>: mov   %fs:0xfffffffffffffff8,%rcx #暂时不关注
  0x000000000044f4e9 <+9>: cmp   0x10(%rcx),%rsp #暂时不关注
  0x000000000044f4ed <+13>: jbe   0x44f51d <main.main+61> #暂时不关注
  0x000000000044f4ef <+15>: sub   $0x20,%rsp #为main函数预留32字节栈空间
  0x000000000044f4f3 <+19>: mov   %rbp,0x18(%rsp) #保存调用者的rbp寄存器
  0x000000000044f4f8 <+24>: lea   0x18(%rsp),%rbp #调整rbp使其指向main函数栈帧开始地址
  0x000000000044f4fd <+29>: movq   $0x1,(%rsp) #sum函数的第一个参数（1）入栈
  0x000000000044f505 <+37>: movq   $0x2,0x8(%rsp) #sum函数的第二个参数（2）入栈
  0x000000000044f50e <+46>: callq 0x44f480 <main.sum> #调用sum函数
  0x000000000044f513 <+51>: mov   0x18(%rsp),%rbp #恢复rbp寄存器的值为调用者的rbp
  0x000000000044f518 <+56>: add   $0x20,%rsp #调整rsp使其指向保存有调用者返回地址的栈单元
  0x000000000044f51c <+60>: retq   #返回到调用者
  0x000000000044f51d <+61>: callq 0x447390 <runtime.morestack_noctxt> #暂时不关注
  0x000000000044f522 <+66>: jmp   0x44f4e0 <main.main> #暂时不关注
```

go函数调用反汇编 main.sum
```text
  0x000000000044f480 <+0>: sub   $0x20,%rsp #为sum函数预留32字节的栈空间
  0x000000000044f484 <+4>: mov   %rbp,0x18(%rsp) #保存main函数的rbp
  0x000000000044f489 <+9>: lea   0x18(%rsp),%rbp #设置sum函数的rbp
  0x000000000044f48e <+14>: movq   $0x0,0x38(%rsp) #返回值初始化为0
  0x000000000044f497 <+23>: mov   0x28(%rsp),%rax #从内存中读取第一个参数a(1)到rax
  0x000000000044f49c <+28>: mov   0x28(%rsp),%rcx #从内存中读取第一个参数a(1)到rcx
  0x000000000044f4a1 <+33>: imul   %rax,%rcx #计算a * a，并把结果放在rcx
  0x000000000044f4a5 <+37>: mov   %rcx,0x10(%rsp) #把rcx的值（a * a）赋值给变量a2
  0x000000000044f4aa <+42>: mov   0x30(%rsp),%rax #从内存中读取第二个参数a(2)到rax
  0x000000000044f4af <+47>: mov   0x30(%rsp),%rcx #从内存中读取第二个参数a(2)到rcx
  0x000000000044f4b4 <+52>: imul   %rax,%rcx #计算b * b，并把结果放在rcx
  0x000000000044f4b8 <+56>: mov   %rcx,0x8(%rsp) #把rcx的值（b * b）赋值给变量b2
  0x000000000044f4bd <+61>: mov   0x10(%rsp),%rax #从内存中读取a2到寄存器rax
  0x000000000044f4c2 <+66>: add   %rcx,%rax #计算a2 + b2,并把结果保存在rax
  0x000000000044f4c5 <+69>: mov   %rax,(%rsp) #把rax赋值给变量c, c = a2 + b2
  0x000000000044f4c9 <+73>: mov   %rax,0x38(%rsp) #将rax的值（a2 + b2）复制给返回值
  0x000000000044f4ce <+78>: mov   0x18(%rsp),%rbp #恢复main函数的rbp
  0x000000000044f4d3 <+83>: add   $0x20,%rsp #调整rsp使其指向保存有返回地址的栈单元
  0x000000000044f4d7 <+87>: retq   #返回main函数
```

函数调用栈关系图
![img_25.png](img_25.png)

### 5.系统调用
```text
系统调用是什么：
根据操作系统为每个API提供的一个整型编号来调用，AMD64 Linux平台约定在进行系统调用时使用rax寄存器存放系统调用编号，同时约定使用rdi, rsi, rdx, r10, r8和r9来传递前6个系统调用参数
*操作系统的代码位于内核地址空间，使用特殊的指令陷入系统内核完成指定的功能
```

go读取文件为例：
```go
package main

import "os"

func main() {
	fd, _ := os.Open("./syscall.go")  // 将会使用系统调用打开文件
	fd.Close()  // 将会使用系统调用关闭文件
}
```

go读取文件反汇编代码
```text
mov    0x10(%rsp),%rdi  #第1个参数
mov    0x18(%rsp),%rsi  #第2个参数
mov    0x20(%rsp),%rdx #第3个参数
mov    0x28(%rsp),%r10 #第4个参数
mov    0x30(%rsp),%r8  #第5个参数
mov    0x38(%rsp),%r9  #第6个参数
mov    0x8(%rsp),%rax  #系统调用编号 rax = 267，表示调用openat系统调用
syscall  #系统调用指令，进入Linux内核

*代码首先把6个参数以及openat这个系统调用的编号267保存在了对应的寄存器中，然后使用syscall指令进入内核执行打开文件的功能。
```

[linux x86_64系统调用](https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md#x86_64-64_bit)

### 6.PThread
What is thread?
See, this is the thread
```c
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#define N (1000 * 1000 * 1000)

volatile int g = 0;

void *start(void *arg)
{
    int i;

    for (i = 0; i < N; i++) {
        g++;
    }

    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t tid;

    // 使用pthread_create函数创建一个新线程执行start函数
    pthread_create(&tid, NULL, start, NULL);

    for (;;) {
        usleep(1000 * 100 * 5);
        printf("loop g: %d\n", g);
        if (g == N) {
            break;
        }
    }

    pthread_join(tid, NULL); // 等待子线程结束运行

    return 0;
}
```

```text
*该程序启动后存在2个线程，一个有操作系统创建的主线程，一个有主线程创建的子线程
*这两个线程同时并发运行在系统中，操作系统负责对它们进行调度

发起调度的时机：
1.用户程序使用系统调用进入操作系统内核
2.时钟中断: 操作系统能够发起抢占调度的基础

调度的核心事项
1.保存当前线程的上下文数据到内存，切换到下一线程
2.从内存中恢复下一线程的上下文，开始执行
*上下文包含：
1.一组通用寄存器的值
2.将要执行的下一条指令的地址
3.栈

总结：操作系统线程是由内核负责调度且拥有自己私有的一组寄存器值和栈的执行流
```

### 7.TLS
```text
TLS是啥：
线程局部存储（线程私有的全局变量）
```

普通全局变量
[./c/global.c](./c/global.c)
```c
#include <stdio.h>
#include <pthread.h>

int g = 0;  // 1，定义全局变量g并赋初值0

void *start(void *arg)
{
    printf("start, g[%p] : %d\n", &g, g); // 4，子线程中打印全局变量g的地址和值

    g++; // 5，修改全局变量

    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t tid;

    g = 100;  // 2，主线程给全局变量g赋值为100

    pthread_create(&tid, NULL, start, NULL); // 3， 创建子线程执行start()函数
    pthread_join(tid, NULL); // 6，等待子线程运行结束

    printf("main, g[%p] : %d\n", &g, g); // 7，打印全局变量g的地址和值

    return 0;
}
```
输出如下，可以看出全局变量g在主子线程中的地址是一样的
```shell
tt@TTdeMacBook-Pro c % gcc -pthread -o global global.c 
tt@TTdeMacBook-Pro c % ./global 
start, g[0x10b162020] : 100
main, g[0x10b162020] : 101
```

线程本地全局变量
[./c/tls0.c](./c/tls0.c)
```c
#include <stdio.h>
#include <pthread.h>

__thread int g = 0;  // 1，定义全局变量g并赋初值0

void *start(void *arg)
{
    printf("start, g[%p] : %d\n", &g, g); // 4，子线程中打印全局变量g的地址和值

    g++; // 5，修改全局变量

    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t tid;

    g = 100;  // 2，主线程给全局变量g赋值为100

    pthread_create(&tid, NULL, start, NULL); // 3， 创建子线程执行start()函数
    pthread_join(tid, NULL); // 6，等待子线程运行结束

    printf("main, g[%p] : %d\n", &g, g); // 7，打印全局变量g的地址和值

    return 0;
}
```

输出结果，全局变量g在主子线程中的地址已经不一致了
```shell
tt@TTdeMacBook-Pro c % gcc -pthread -o tls0 tls0.c
tt@TTdeMacBook-Pro c % ./tls0
start, g[0x7ff426405a30] : 0
main, g[0x7ff426405a20] : 100
```

main函数反汇编
```text
   0x00000000004006e8 <+0>:     push   %rbp
   0x00000000004006e9 <+1>:     mov    %rsp,%rbp
=> 0x00000000004006ec <+4>:     sub    $0x20,%rsp
   0x00000000004006f0 <+8>:     mov    %edi,-0x14(%rbp)
   0x00000000004006f3 <+11>:    mov    %rsi,-0x20(%rbp)
   0x00000000004006f7 <+15>:    movl   $0x64,%fs:0xfffffffffffffffc //@@ g的地址是%fs:0xfffffffffffffffc
   0x0000000000400703 <+27>:    lea    -0x8(%rbp),%rax
   0x0000000000400707 <+31>:    mov    $0x0,%ecx
   0x000000000040070c <+36>:    mov    $0x400696,%edx
   0x0000000000400711 <+41>:    mov    $0x0,%esi
   0x0000000000400716 <+46>:    mov    %rax,%rdi
   0x0000000000400719 <+49>:    callq  0x400580 <pthread_create@plt>
   0x000000000040071e <+54>:    mov    -0x8(%rbp),%rax
   0x0000000000400722 <+58>:    mov    $0x0,%esi
   0x0000000000400727 <+63>:    mov    %rax,%rdi
   0x000000000040072a <+66>:    callq  0x4005a0 <pthread_join@plt>
```

start函数的反汇编
```text
   0x0000000000400696 <+0>:     push   %rbp
   0x0000000000400697 <+1>:     mov    %rsp,%rbp
=> 0x000000000040069a <+4>:     sub    $0x10,%rsp
   0x000000000040069e <+8>:     mov    %rdi,-0x8(%rbp)
   0x00000000004006a2 <+12>:    mov    %fs:0xfffffffffffffffc,%eax //@@ g的地址是%fs:0xfffffffffffffffc
   0x00000000004006aa <+20>:    mov    %fs:0x0,%rdx
   0x00000000004006b3 <+29>:    lea    -0x4(%rdx),%rcx
   0x00000000004006ba <+36>:    mov    %eax,%edx
   0x00000000004006bc <+38>:    mov    %rcx,%rsi
   0x00000000004006bf <+41>:    mov    $0x400808,%edi
   0x00000000004006c4 <+46>:    mov    $0x0,%eax
   0x00000000004006c9 <+51>:    callq  0x400590 <printf@plt>
   0x00000000004006ce <+56>:    mov    %fs:0xfffffffffffffffc,%eax
   0x00000000004006d6 <+64>:    add    $0x1,%eax
   0x00000000004006d9 <+67>:    mov    %eax,%fs:0xfffffffffffffffc
   0x00000000004006e1 <+75>:    mov    $0x0,%eax
```

```text
1.通过如上对2个函数反汇编看到g的地址均是用 %fs:0xfffffffffffffffc 来表示
2.在上面的输出中g在主子线程的地址是不一样的，那只能说明 这里的%fs在不同的线程下其值是不一样的
```

在主子线程中均打印fs的地址
[./c/tls.c](./c/tls.c)
```c
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <asm/prctl.h>
#include <sys/prctl.h>

__thread int g = 0;

void print_fs_base() {
    unsigned long addr;
    int ret = arch_prctl(ARCH_GET_FS, &addr);  //获取fs段基地址
    if (ret < 0) {
        perror("error");
        return;
    }

    printf("fs base addr: %p\n", (void *) addr); //打印fs段基址

    return;
}

void *start(void *arg) {
    print_fs_base(); //子线程打印fs段基地址
    printf("start, g[%p] : %d\n", &g, g);

    g++;

    return NULL;
}

int main(int argc, char *argv[]) {
    pthread_t tid;

    g = 100;

    pthread_create(&tid, NULL, start, NULL);
    pthread_join(tid, NULL);

    print_fs_base(); //main线程打印fs段基址
    printf("main, g[%p] : %d\n", &g, g);

    return 0;
}
```
输出结果
```shell
[root@78aa7d637940 c]# gcc -pthread -o tls tls.c
[root@78aa7d637940 c]# ./tls
fs base addr: 0x7f123c251700
start, g[0x7f123c2516fc] : 0
fs base addr: 0x7f123ca5b740
main, g[0x7f123ca5b73c] : 100
```
```text
以上输出确认fs段地址在主子线程中是不一致的
总结：gcc编译器（其实还有线程库以及内核的支持）使用了CPU的fs段寄存器来实现线程本地存储
```