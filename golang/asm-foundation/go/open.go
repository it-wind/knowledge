package main

import "os"

func main() {
	fd, _ := os.Open("./syscall.go")  // 将会使用系统调用打开文件
	fd.Close()  // 将会使用系统调用关闭文件
}