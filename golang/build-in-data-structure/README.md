### 1.内置数据结构
```text
runtime
    channel
    timer
    semaphore
    map
    iface
    eface
    slice
    string

sync
    mutex
    cond
    pool
    once
    map
    waitgroup

container 没有范型，各种interface需要断言，不常用
    heap
    list
    ring

netpoll
    netpoll related

memory
    allocation related
    gc related

os
    os related

context
    context
```

### 2.channel
```go
package runtime
type hchan struct {
	qcount   uint           // channel中目前存在的元素的个数
	dataqsiz uint           // channel 长度
	buf      unsafe.Pointer // 环形数组，存放元素
	elemsize uint16
	closed   uint32
	elemtype *_type // element type
	sendx    uint   // 发送数据时写入buf的索引位置
	recvx    uint   // 接收数据时读取buf的索引位置
	recvq    waitq  // 接收等待队列（链表）：当接收数据时，buf中数据为空时，当前的g会被封装成sudog挂在该链表上
	sendq    waitq  // 发送等待队列（链表）：当发送数据时，buf中数据装满时，当前的g会被封装成sudog挂在该链表上
	lock mutex
}
```

### 3.map

### 4.context

### 5.等等慢慢补充