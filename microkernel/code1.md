```nasm
;处理器的工作模式是将内存分成逻辑上的段,
;指令和数据的访问一律按“段地址:偏移地址”的方式进行。
;相对应地，一个规范的程序，应当包括代码段、数据段、栈段。


;以下显示字符串"Hello World!"
;文本模式下用2个字节显示一个字符：第一个1字节是字符本身，第二个字节是显示属性，比如颜色，闪烁等
;把要显示的数据写入到 0xb800:0x00 ~ 0xb800:0x16处
;当操作内存中的数据格式：[段：偏移], 若段省略则默认是 [offset] === [DS:offset]


;标号作用：
;格式: 自定义标号名: ，其中的冒号可省略
;可以被其他指令当作操作数使用，在编译阶段会被替换成汇编地址

;SECTION 定义一个段
;code 自定义段的名字，可以自定义
;align 汇编地址对齐方式，可以是16的倍数
;vstart 当段定义不存在vstart时，标号是从程序开头到标号所在的指令处偏移计算汇编地址
;vstart 当段定义存在vstart时，标号是从段头到标号所在指令的偏移加上vstart计算汇编地址
;直到下一个SECTION出现之前的所有代码都属于本段
SECTION code align=16 vstart=0x7c00 ;本段中所用到的标号都会加上0x7c00
    ;跳过数据定义
    ;start 是一个标号
    ;跳转到start标号处开始执行
    jmp near start ;该条指令就在内存0x7c00处

    ;mytext 是一个标号, 在代码中有用到mytext的地方在编译阶段会被替换成mytext定义时所在位置的汇编地址
    ;db 是定义多个存放一字节长度的数据的指令
    mytext db 'hello world'
    mytext2 db 'i am it-wind'
    ;用于保存光标位置
    cursor dw 0
    
;start标号在这里，从这里开始执行
start:
    ;mov 传送指令 等价于 ax=0xb800
    mov ax,0xb800 ;指向文本模式的显示缓冲区
    mov es,ax

    ;设置数据段的基地址
    ;把数据段和代码段设置成一样，需要操作的数据在代码中
    ;代码段中也是可以有数据的，只需要确保不被cpu当作指令执行到即可
    mov cx,cs
    mov ds,cx

    ;过程调用的参数, 期望显示字符串的ds段内的偏移地址
    mov bx,mytext
    ;过程调用的参数, 期望显示字符串的长度
    mov cx,mytext2-mytext
    ;过程调用指令，过程执行完会返回到该处继续执行下一条指令
    call put_string_ln

    mov bx,mytext2
    mov cx,cursor-mytext2
    call put_string_ln

    jmp near $                 ;无限循环

;在屏幕上打印输出
;输入：DS:BX=串地址
;输入：CX=串长度
put_string_ln:
    ;确保不污染原始寄存器的值，先将其入栈，后再还原
    push dx
    push di
    push ax

    ;获取光标
    mov di, [cs:cursor]

.put_char:
    ;获取输出的单个字符
    mov dl, [bx] ;[bx] === [ds:bx]
    mov dh, 0x07
    ;放入到显示区
    mov word [es:di], dx

    ;指向下一个输出字符的地址
    inc bx
    ;光标后移
    inc di
    inc di

    ;当cx不为0则跳转到标号.put_char处继续执行
    ;当cx为0该指令不生效，继续向下执行
    loop .put_char

    ;输出换行 - 把光标定位到下一行的第一个字符
    ;每行能输出80个字符 = 160个字节, 补满80个字符
    mov dl, 160

;用空字符填充
.full_zero:
    ;若光标所在位置整除160，即跳过填充
    mov ax, di
    div dl
    cmp ah, 0
    jz .put_string_ln_end
    mov byte [es:di], ''
    inc di
    mov byte [es:di], 0x07
    inc di
    jmp .full_zero
    
;过程即将结束，进行收尾工作
.put_string_ln_end:
    ;光标写入内存
    mov [cs:cursor], di
    ;还原寄存器
    pop dx
    pop di
    pop ax
    ;返回
    ret

    ;重复指令 db 
    ;不满510个字节，填充至510个字节
    times 510-($-$$) db 0
    ;最后2个字节必须是 0x55,0xaa 引导扇区的特定标识
    db 0x55,0xaa
```