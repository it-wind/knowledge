```nasm
;不同操作系统加载器是明确了被加载程序的格式，否则不识别将不会被执行
;加载器会加载以下程序到指定空闲内存
;并设置ds、es指向被加载程序的header段
;跳转到entry处保存的程序入口进行执行

;用户程序
;自定义的程序格式，只能够被我们的加载器识别
;程序的目的是在屏幕上输出: "i am id-wind"
;请思考本程序能否正确执行?

;程序被加载到内存中从上至下
;上面 -> 内存的低地址
;下面 -> 内存的高地址

;程序头部
section header align=16 vstart=0
    ;程序长度
    length dd program_end
    ;程序入口
    ;程序的代码段内偏移 - 入口
    entry dw start
    ;程序的代码段
    dd section.code.start
    
    ;程序的代码段
    code_segment dd section.code.start
    ;程序的数据段
    data_segment dd section.data.start
    ;程序的栈段
    stack_segment dd section.stack.start

;代码段    
section code align=16 vstart=0
;入口
start:
    ;本程序是被加载器加载到内存中并从此处开始执行
    ;加载器在执行该入口前会把DS和ES指向本程序头部段
    ;确保本程序的正确执行，先考虑把DS/ES/SS指向
    
    ;ss指向本程序的栈段
    mov ax, [stack_segment] ;[header:stack_segment]
    mov ss, ax
    mov sp, stack_end
    
    ;ds指向本程序的数据段
    mov ax, [data_segment] ;[header:data_segment]
    mov ds, ax
    
    mov ax,0xb800 ;指向文本模式的显示缓冲区
    mov es,ax
    
    ;ds已经设置为section.data.start
    ;[my_text1] === [ds:my_text1]
    mov bx, my_text1
    mov cx, program_end-my_text1
    call put_string_ln
    
;在屏幕上打印输出
;输入：DS:BX=串地址
;输入：CX=串长度
put_string_ln:
    ;确保不污染原始寄存器的值，先将其入栈，后再还原
    push dx
    push di
    push ax

    ;获取光标
    mov di, [cursor]

.put_char:
    ;获取输出的单个字符
    mov dl, [bx]
    mov dh, 0x07
    ;放入到显示区
    mov word [es:di], dx

    ;指向下一个输出字符的地址
    inc bx
    ;光标后移
    inc di
    inc di

    ;当cx不为0则跳转到标号.put_char处继续执行
    ;当cx为0该指令不生效，继续向下执行
    loop .put_char

    ;输出换行 - 把光标定位到下一行的第一个字符
    ;每行能输出80个字符 = 160个字节, 补满80个字符
    mov dl, 160

;用空字符填充
.full_zero:
    ;若光标所在位置整除160，即跳过填充
    mov ax, di
    div dl
    cmp ah, 0
    jz .put_string_ln_end
    mov byte [es:di], ''
    inc di
    mov byte [es:di], 0x07
    inc di
    jmp .full_zero
    
;过程即将结束，进行收尾工作
.put_string_ln_end:
    ;光标写入内存
    mov [cursor], di
    ;还原寄存器
    pop dx
    pop di
    pop ax
    ;返回
    ret

;数据段
section data align=16 vstart=0
    ;输出文本
    mytext1 db 'i am it-wind'
    ;用于保存光标位置
    cursor dw 0

;栈段
section stack align=16 vstart=0
    ;保留256个字节的空间给栈段使用
    resb 256
stack_end:

;单独定义一个尾段，主要是为了定义标号program_end
;因为本段的定义未指定vstart,所以标号是从程序的开头开始计算汇编地址
section tail align=16 
;程序结束的标号，用于计算程序的长度，用于完整的加载该程序
program_end:
```