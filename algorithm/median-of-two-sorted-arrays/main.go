package main

import (
	"fmt"
)

type list struct {
	first     *node
	tail      *node
	free      *node
	len       int
	cap       int
	pushCount int
}

type node struct {
	next *node
	val  int
}

type list2 struct {
	data []int
	i    int
}

func NewList2(c int) *list2 {
	return &list2{
		data: make([]int, c),
		i:    0,
	}
}

func (l *list2) push(d int) {
	if l.i == len(l.data) {
		l.i = 0
	}
	l.data[l.i] = d
	l.i++
}

func (l *list2) avg() float64 {
	r := 0
	for _, v := range l.data {
		r += v
	}
	return float64(r) / float64(len(l.data))
}

func NewList(c int) *list {
	l := &list{cap: c}
	if c > 0 {
		tmNode := new(node)
		l.free = tmNode
		for i := 1; i < c; i++ {
			tmNode.next = new(node)
			tmNode = tmNode.next
		}
	}
	return l
}

func (l *list) GetNode(val int) *node {
	if l.free == nil {
		return &node{val: val}
	}
	r := l.free
	l.free = l.free.next
	r.next = nil
	r.val = val
	return r
}

func (l *list) PutNode(n *node) {
	if l.free == nil {
		l.free = n
		n.next = nil
		return
	}
	n.next = l.free
	l.free = n
}

func (l *list) push(d int) {
	fmt.Println("push:", d)
	l.pushCount++
	if l.len == 0 {
		l.first = l.GetNode(d)
		l.tail = l.first
		l.len = 1
		return
	}

	l.tail.next = l.GetNode(d)
	l.tail = l.tail.next
	if l.len == l.cap {
		tmNode := l.first
		l.first = l.first.next
		l.PutNode(tmNode)
	} else {
		l.len++
	}
}

func (l *list) avg() float64 {
	if l.first == nil {
		return 0
	}

	var r int
	tmNode := l.first
	for tmNode != nil {
		fmt.Println("val:", tmNode.val)
		r += tmNode.val
		tmNode = tmNode.next
	}
	return float64(r) / float64(l.len)
}

func findMedianSortedArray(nums1 []int) float64 {
	n1Len := len(nums1)
	if n1Len == 0 {
		return 0
	}
	if n1Len%2 == 0 {
		f := nums1[n1Len/2-1]
		s := nums1[n1Len/2]
		return float64(f+s) / 2
	}
	return float64(nums1[n1Len/2])
}

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	n1Len := len(nums1)
	n2Len := len(nums2)

	if n1Len == 0 {
		return findMedianSortedArray(nums2)
	}
	if n2Len == 0 {
		return findMedianSortedArray(nums1)
	}

	c := 1
	if (n1Len+n2Len)%2 == 0 {
		c = 2
	}
	l := NewList2(c)
	end := (n1Len + n2Len) / 2

	i := 0
	j := 0
	for {
		if end == i+j-1 {
			break
		}

		if i == n1Len {
			if j == n2Len {
				break
			}
			l.push(nums2[j])
			j++
			continue
		}
		if j == n2Len {
			l.push(nums1[i])
			i++
			continue
		}

		if nums1[i] < nums2[j] {
			l.push(nums1[i])
			i++
		} else {
			l.push(nums2[j])
			j++
		}
	}
	return l.avg()
}

func main() {
	m := findMedianSortedArrays([]int{0, 0, 0, 0, 0}, []int{-1, 0, 0, 0, 0, 0, 1})
	fmt.Println(m)
}
