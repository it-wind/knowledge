package main

import (
	"fmt"
	"math/rand"
	"time"
)

var cycle *link

type link struct {
	next *link
	val  int
}

func (l *link) isCycle() bool {
	slow, fast := l, l
	for slow != nil && fast != nil && fast.next != nil {
		slow = slow.next
		fast = fast.next.next
		if slow == fast {
			return true
		}
	}
	return false
}

func (l *link) view() {
	var stop bool
	var tmpArr []int
	tmpLink := l
	for tmpLink != nil {
		if tmpLink == cycle {
			if stop {
				break
			}
			stop = true
		}
		tmpArr = append(tmpArr, tmpLink.val)
		tmpLink = tmpLink.next
	}

	if cycle == nil {
		fmt.Println("link_view:", tmpArr, nil)
	} else {
		fmt.Println("link_view:", tmpArr, cycle.val)
	}
}

func NewLink(d int) *link {
	return &link{val: d}
}

func convertLink(l []int) *link {
	var tmpLink *link
	var pre *link
	var r *link
	rand.Seed(time.Now().UnixNano())
	for i, i2 := range l {
		tmpLink = NewLink(i2)
		if pre != nil {
			pre.next = tmpLink
			pre = tmpLink
		}
		if i == 0 {
			r = tmpLink
			pre = tmpLink
		}
		if cycle == nil && rand.Intn(len(l)) == 1 {
			cycle = tmpLink
		}
	}

	if tmpLink != nil {
		tmpLink.next = cycle
	}
	return r
}

func main() {
	l := convertLink([]int{1, 2, 3, 4, 5, 6, 7, 8, 9})
	l.view()

	fmt.Println(l.isCycle())
}
