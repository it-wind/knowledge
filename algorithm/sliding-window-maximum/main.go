package main

import "fmt"

type queue struct {
	input  *stack
	output *stack
	len    int
}

type stack struct {
	base []int32
	top  int
}

func NewStack(defaultSize int) *stack {
	if defaultSize == 0 {
		defaultSize = 64
	}
	return &stack{base: make([]int32, 0, defaultSize)}
}

func (s *stack) isEmpty() bool {
	return s.top == 0
}

func (s *stack) pop() int32 {
	if s.top == 0 {
		return 0
	}
	s.top--
	r := s.base[s.top]
	return r
}

func (s *stack) view() {
	fmt.Printf("%#v\n", s.base[:s.top])
}

func (s *stack) push(d int32) {
	if len(s.base) > s.top {
		s.base[s.top] = d
	} else {
		s.base = append(s.base, d)
	}
	s.top++
}

func NewQueue() *queue {
	return &queue{
		input:  NewStack(0),
		output: NewStack(0),
	}
}

func (q *queue) push(d int32) {
	q.len++
	q.input.push(d)
}

func (q *queue) pop() int32 {
	if q.output.isEmpty() {
		for !q.input.isEmpty() {
			q.output.push(q.input.pop())
		}
	}

	q.len--
	if q.len < 0 {
		q.len = 0
	}
	return q.output.pop()
}

func (q *queue) isEmpty() bool {
	return q.len == 0
}

func maxSlidingWindow(s []int) (r []int) {
	return
}

func main() {
	fmt.Println(maxSlidingWindow([]int{1, 3, -1, -3, 5, 3, 6}))
}
