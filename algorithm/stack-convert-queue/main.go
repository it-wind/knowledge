package main

import (
	"fmt"
)

type MyQueue struct {
	input  *stack
	output *stack
	len    int
}

type stack struct {
	base []int
	top  int
}

func NewStack(defaultSize int) *stack {
	if defaultSize == 0 {
		defaultSize = 64
	}
	return &stack{base: make([]int, 0, defaultSize)}
}

func (s *stack) isEmpty() bool {
	return s.top == 0
}

func (s *stack) pop() int {
	if s.top == 0 {
		return 0
	}
	s.top--
	r := s.base[s.top]
	return r
}

func (s *stack) view() {
	fmt.Printf("%#v\n", s.base[:s.top])
}

func (s *stack) push(d int) {
	if len(s.base) > s.top {
		s.base[s.top] = d
	} else {
		s.base = append(s.base, d)
	}
	s.top++
}

func Constructor() MyQueue {
	return MyQueue{
		input:  NewStack(0),
		output: NewStack(0),
	}
}

func (q *MyQueue) Push(d int) {
	q.len++
	q.input.push(d)
}

func (q *MyQueue) Peek() int {
	if !q.output.isEmpty() {
		top := q.output.top - 1
		return q.output.base[top]
	}
	if !q.input.isEmpty() {
		return q.input.base[0]
	}
	return 0
}

func (q *MyQueue) Pop() int {
	if q.output.isEmpty() {
		for !q.input.isEmpty() {
			q.output.push(q.input.pop())
		}
	}

	q.len--
	if q.len < 0 {
		q.len = 0
	}
	return q.output.pop()
}

func (q *MyQueue) Empty() bool {
	return q.len == 0
}

func intArrToQueue(a []int) *MyQueue {
	q := Constructor()
	for _, i2 := range a {
		q.Push(i2)
	}
	return &q
}

func main() {
	q := intArrToQueue([]int{1, 2})
	fmt.Println(q.Peek())
	fmt.Println(q.Pop())
}
