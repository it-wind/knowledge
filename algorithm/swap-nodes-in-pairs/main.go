package main

import "fmt"

type link struct {
	next *link
	val  int
}

func (l *link) revertSwapLink() *link {
	if l == nil {
		return nil
	}

	nullRoot := NewLink(0)
	nullRoot.next = l
	prev, cur, next := nullRoot, l, l.next
	for cur != nil && next != nil {
		prev.next = next
		prev = cur

		cur.next = next.next
		next.next = cur

		cur = cur.next
		if cur != nil {
			next = cur.next
		}
	}
	return nullRoot.next
}

func (l *link) view() {
	var tmpArr []int
	tmpLink := l
	for tmpLink != nil {
		tmpArr = append(tmpArr, tmpLink.val)
		tmpLink = tmpLink.next
	}
	fmt.Println("link_view:", tmpArr)
}

func NewLink(d int) *link {
	return &link{val: d}
}

func convertLink(l []int) *link {
	var pre *link
	var r *link
	for i, i2 := range l {
		tmpLink := NewLink(i2)
		if pre != nil {
			pre.next = tmpLink
			pre = tmpLink
		}
		if i == 0 {
			r = tmpLink
			pre = tmpLink
		}
	}
	return r
}

func main() {
	l := convertLink([]int{1, 2, 3, 4, 5, 6})
	l.view()

	l2 := l.revertSwapLink()
	l2.view()
}
