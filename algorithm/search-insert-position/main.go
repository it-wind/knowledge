package main

import (
	"fmt"
)

func searchInsert(nums []int, target int) int {
	if len(nums) == 0 {
		return 0
	}
	if len(nums) == 1 {
		if nums[0] >= target {
			return 0
		}
		return 1
	}
	mx := len(nums) / 2
	if nums[mx] == target {
		return mx
	}
	if nums[mx] > target {
		return searchInsert(nums[:mx], target)
	}
	return searchInsert(nums[mx:], target) + mx
}

func main() {
	fmt.Println(searchInsert([]int{1}, -1))
}
