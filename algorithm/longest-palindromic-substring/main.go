package main

import (
	"fmt"
)

func longestPalindrome(s string) string {
	if len(s) == 0 {
		return ""
	}

	ss := make(map[byte][]int)
	for i := 0; i < len(s); i++ {
		ss[s[i]] = append(ss[s[i]], i)
	}

	var f, l int
	for i := 0; i < len(s)-1; i++ {
		//lastIndex := len(s)
		indexList := ss[s[i]]
		indexListLast := len(indexList) - 1
		for indexList[indexListLast] > i {
			//index := strings.LastIndexByte(s[i+1:lastIndex], s[i]) + i + 1
			//if index == -1 {
			//	break
			//}
			index := indexList[indexListLast]
			if index-i <= l-f {
				break
			}
			indexListLast--
			//lastIndex = index
			head := i
			tail := index
			for head < tail {
				head++
				tail--
				if s[head] != s[tail] {
					break
				}
			}
			if s[head] == s[tail] && index-i > l-f {
				f = i
				l = index
			}
		}
	}
	return s[f : l+1]
}

func main() {
	fmt.Println(longestPalindrome("abccba"))
}
