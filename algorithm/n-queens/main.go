package main

import (
	"bytes"
	"fmt"
)

func main() {
	fmt.Println(solveNQueens(4))
	fmt.Println(res)
}

var res [][]int

func solveNQueens(n int) [][]string {
	res = make([][]int, 0)

	dfs(0, n, make([]int, n))

	ret := make([][]string, 0)
	for _, is := range res {
		r := make([]string, len(is))
		for i2, i3 := range is {
			resRow := bytes.Repeat([]byte("."), n)
			resRow[i3] = 'Q'
			r[i2] = string(resRow)
		}
		ret = append(ret, r)
	}
	return ret
}

var pie = make(map[int]struct{})
var na = make(map[int]struct{})
var col = make(map[int]struct{})

func dfs(row, n int, result []int)  {
	if row == n {
		result1 := make([]int, n)
		copy(result1, result)
		res = append(res, result1)
		return
	}

	for i := 0; i < n; i++ {
		if _, e := col[i]; e {
			continue
		}
		if _, e := na[row-i]; e {
			continue
		}
		if _, e := pie[row+i]; e {
			continue
		}

		result[row] = i
		col[i] = struct{}{}
		na[row-i] = struct{}{}
		pie[row+i] = struct{}{}

		//if row+1 == n {
		//	fmt.Println(row, i, result)
		//}
		dfs(row+1, n, result)

		delete(col, i)
		delete(na, row-i)
		delete(pie, row+i)
	}
}