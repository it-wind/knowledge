package main

import (
	"fmt"
	"gitee.com/it-wind/knowledge/course/data-struct/deque"
)

func main()  {
	graph := make(map[string]map[string]int)

	graph["乐谱"] = make(map[string]int)
	graph["乐谱"]["黑胶唱片"] = 5
	graph["乐谱"]["海报"] = 0

	graph["黑胶唱片"] = make(map[string]int)
	graph["黑胶唱片"]["低音吉他"] = 15
	graph["黑胶唱片"]["架子鼓"] = 20

	graph["海报"] = make(map[string]int)
	graph["海报"]["低音吉他"] = 30
	graph["海报"]["架子鼓"] = 35

	graph["低音吉他"] = make(map[string]int)
	graph["低音吉他"]["钢琴"] = 20

	graph["架子鼓"] = make(map[string]int)
	graph["架子鼓"]["钢琴"] = 10

	fmt.Println(Dijkstra(graph, "乐谱", "钢琴"))
}

func NewGraph(v, e int) (graph map[string]map[string]int) {
	return
}

func Dijkstra(graph map[string]map[string]int, start, end string) (r []string) {
	cost := make(map[string]struct{
		Parent string
		Cost int
	})

	dq := deque.NewQueue()
	dq.LPush(start)

	processed := make(map[string]struct{})

	for dq.Len() > 0 {
		_, g := dq.RPop()
		t1 := g.(string)
		if _, ok := processed[t1]; ok {
			//continue
		}

		for k, v := range graph[t1] {
			c, ok := cost[k]
			if !ok {
				cost[k] = struct {
					Parent string
					Cost   int
				}{Parent: t1, Cost: v + cost[t1].Cost}
			} else {
				newCost := cost[t1].Cost + v
				if newCost < c.Cost {
					c.Parent = t1
					c.Cost = newCost
					cost[k] = c
				}
			}
			processed[t1] = struct{}{}
			dq.LPush(k)
		}
	}

	fmt.Printf("%v", cost)

	return
}
