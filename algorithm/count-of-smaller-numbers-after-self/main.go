package main

import (
	"fmt"
)

func main() {
	//fmt.Println(countSmaller([]int{5,56,6,7}))
	fmt.Println(countSmaller([]int{26, 78, 27, 100, 33, 67, 90, 23, 66, 5, 38, 7, 35, 23, 52, 22}))
	//fmt.Println(countSmaller([]int{5,2,6,1}))}
}

//func countSmaller(nums []int) []int {
//	ret := make([]int, len(nums))
//
//	numsIndex := make([][2]int, len(nums))
//	for i := 0; i < len(nums); i++ {
//		numsIndex[i] = [2]int{
//			nums[i],i,
//		}
//	}
//	gb(numsIndex, ret, 0, len(nums) - 1)
//	return ret
//}
//
//func gb(numsIndex [][2]int, ret []int, start ,end int) {
//	if end - start == 0 {
//		return
//	}
//
//	count := end - start + 1
//	mid := count / 2 + start - 1
//
//	//分离
//	gb(numsIndex, ret, start, mid)
//	gb(numsIndex, ret, mid + 1, end)
//
//	addNums := make([]int, 0)
//	newNumsItem := make([][2]int, count)
//	newNumsIndex := 0
//
//	//合并
//	i := mid + 1
//	j := start
//	for i <= end && j <= mid {
//		if numsIndex[j][0] <= numsIndex[i][0] {
//			newNumsItem[newNumsIndex] = numsIndex[j]
//			j ++
//		} else {
//			newNumsItem[newNumsIndex] = numsIndex[i]
//			addNums = append(addNums, j)
//			i ++
//		}
//		newNumsIndex ++
//	}
//
//	//剩下的数据无须比较直接合并到新切片
//	for ; i <= end; i++ {
//		newNumsItem[newNumsIndex] = numsIndex[i]
//		newNumsIndex ++
//	}
//	for ; j <= mid; j++ {
//		newNumsItem[newNumsIndex] = numsIndex[j]
//		newNumsIndex ++
//	}
//
//	//处理ret计数
//	if len(addNums) > 0 {
//		sort.Ints(addNums)
//		addNumsMap := make(map[int]int)
//		for k, v := range addNums {
//			addNumsMap[v] = k + 1
//		}
//		for i := addNums[0]; i <= mid; i++ {
//			if _, ok := addNumsMap[i]; !ok {
//				addNumsMap[i] = addNumsMap[i - 1]
//			}
//		}
//		for k, v := range addNumsMap {
//			ret[numsIndex[k][1]] += v
//		}
//	}
//
//	//把新的切片复制到原切片
//	for i2, item := range newNumsItem {
//		numsIndex[i2+start] = item
//	}
//}

func mergeSort(indexArr []int, nums, result []int) {
	if len(indexArr) <= 1 {
		return
	}
	n := len(indexArr)
	m := n >> 1
	mergeSort(indexArr[:m], nums, result)
	mergeSort(indexArr[m:], nums, result)
	var (
		l = 0
		r    = m
		temp = make([]int, 0, n)
	)
	var count int
	for l < m && r < n {
		if nums[indexArr[l]] <= nums[indexArr[r]] {
			temp = append(temp, indexArr[l])
			result[indexArr[l]] += count
			l++
		} else {
			temp = append(temp, indexArr[r])
			r++
			count++
		}
	}
	for l < m {
		temp = append(temp, indexArr[l])
		result[indexArr[l]] += count
		l++
	}
	for r < n {
		temp = append(temp, indexArr[r])
		r++
	}
	copy(indexArr, temp)
}

func countSmaller(nums []int) []int {
	if len(nums) == 0 {
		return []int{0}
	}
	result := make([]int, len(nums))
	index := make([]int, 0, len(nums))
	for i := 0; i < len(nums); i++ {
		index = append(index, i)
	}
	mergeSort(index, nums, result) // fmt.Println(index)
	return result
}