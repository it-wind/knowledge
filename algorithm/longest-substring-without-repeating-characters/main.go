package main

import "fmt"

func lengthOfLongestSubstring(s string) int {
	sLen := len(s)
	if sLen == 0 {
		return 0
	}
	if sLen == 1 {
		return 1
	}

	max := 0
	start := 0
	index := 0
	sMap := map[uint8]int{}
	for i := 0; i < sLen; i++ {
		var ok bool
		index, ok = sMap[s[i]]
		if ok {
			if max < len(sMap) {
				max = len(sMap)
			}
			for j := start; j <= index; j++ {
				delete(sMap, s[j])
			}
			start = index + 1
		}
		sMap[s[i]] = i
	}
	if max < len(sMap) {
		return len(sMap)
	}
	return max
}

func main() {
	max := lengthOfLongestSubstring("abcabcbb")
	fmt.Println(max)
}
