package main

import (
	"fmt"
	"math"
)

func main()  {
	fmt.Println(maxProduct([]int{-1,-2,-9,-6}))
}

func maxProduct(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	var res int
	max, min := make([]int, len(nums)), make([]int, len(nums))
	max[0], min[0], res = nums[0], nums[0], nums[0]
	for i := 1; i < len(nums); i++ {
		maxTmp := float64(max[i-1] * nums[i])
		minTmp := float64(min[i-1] * nums[i])

		maxTmp, minTmp = math.Max(maxTmp, minTmp), math.Min(maxTmp, minTmp)

		max[i] = int(math.Max(maxTmp, float64(nums[i])))
		min[i] = int(math.Min(minTmp, float64(nums[i])))

		res = int(math.Max(float64(res), float64(max[i])))
	}
	fmt.Println(min,max)
	return res
}