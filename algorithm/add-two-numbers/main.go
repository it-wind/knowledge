package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func add(l1 *ListNode, l2 *ListNode, jw int) (rl1 *ListNode, rl2 *ListNode, s int, rjw int) {
	var l1Val, l2Val int
	if l1 != nil {
		rl1 = l1.Next
		l1Val = l1.Val
	}
	if l2 != nil {
		rl2 = l2.Next
		l2Val = l2.Val
	}
	s = l1Val + l2Val + jw
	if s > 9 {
		s %= 10
		rjw = 1
	}
	return
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	var s int
	jw := 0
	r := new(ListNode)
	n := r
	for {
		l1, l2, s, jw = add(l1, l2, jw)
		n.Val = s
		if l1 != nil || l2 != nil {
			n.Next = new(ListNode)
			n = n.Next
			continue
		} else if jw == 1 {
			n.Next = new(ListNode)
			n.Next.Val = 1
		}
		break
	}
	return r
}

func createList(x []int) *ListNode {
	r := new(ListNode)
	n := r
	n.Val = x[0]
	for i := 1; i < len(x); i++ {
		n.Next = new(ListNode)
		n.Next.Val = x[i]
		n = n.Next
	}
	return r
}

func viewList(x *ListNode) {
	fmt.Println("view list node: ")
	n := x
	for n != nil {
		fmt.Println(n.Val)
		n = n.Next
	}

}

func main() {
	l1 := createList([]int{3, 4, 5, 6})
	l2 := createList([]int{6, 7, 8, 9})

	l3 := addTwoNumbers(l1, l2)
	viewList(l3)
}
