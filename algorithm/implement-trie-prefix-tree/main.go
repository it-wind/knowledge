package main

import "fmt"

func main() {
	trie := Constructor()
	trie.Insert("hello")
	trie.Insert("app")
	trie.Insert("apps")
	trie.Insert("add")
	fmt.Println(trie.Search("hello"))
	fmt.Println(trie.StartsWith("he"))
	fmt.Println(trie.Search("app"))
	trie.Del("hello")
	trie.Del("app")
	fmt.Println(trie.Search("apps"))
	fmt.Println(trie.Search("hello"))

}

type Trie struct {
	isWord bool
	child [26]*Trie
	childLen int
}


func Constructor() Trie {
	return Trie{}
}


func (this *Trie) Insert(word string)  {
	if word == "" {
		return
	}
	u := word[0] - 'a'
	if this.child[u] == nil {
		this.childLen ++
		this.child[u] = &Trie{}
	}
	if len(word) == 1 {
		this.child[u].isWord = true
	}
	this.child[u].Insert(word[1:])
}


func (this *Trie) Search(word string) bool {
	if word == "" {
		return false
	}
	u := word[0] - 'a'
	if this.child[u] == nil {
		return false
	}
	if len(word) == 1 && this.child[u].isWord {
		return true
	}
	return this.child[u].Search(word[1:])
}


func (this *Trie) StartsWith(prefix string) bool {
	if prefix == "" {
		return false
	}
	u := prefix[0] - 'a'
	if this.child[u] == nil {
		return false
	}
	if len(prefix) == 1 {
		return true
	}
	return this.child[u].StartsWith(prefix[1:])
}

func (this *Trie) Del(word string) bool {
	return this._del(word, word, make([]*Trie, 0))
}

func (this *Trie) _del(word, srcWord string, m []*Trie) bool {
	if word == "" {
		return false
	}
	u := word[0] - 'a'
	if this.child[u] == nil {
		return false
	}
	m = append(m, this)
	if len(word) == 1 && this.child[u].isWord {
		this.child[u].isWord = false
		if this.child[u].childLen > 0 {
			return true
		}

		for i := len(m) - 1; i >= 0; i-- {
			if m[i].childLen == 0 && m[i].isWord == false {
				m[i].child[srcWord[i]] = nil
			}
		}
	}
	return this.child[u]._del(word[1:], srcWord, m)
}

/**
 * Your Trie object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Insert(word);
 * param_2 := obj.Search(word);
 * param_3 := obj.StartsWith(prefix);
 */