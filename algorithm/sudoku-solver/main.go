package main

import (
	"fmt"
)

func main() {
	board := [][]byte{
		{'5','3','.','.','7','.','.','.','.'},
		{'6','.','.','1','9','5','.','.','.'},
		{'.','9','8','.','.','.','.','6','.'},
		{'8','.','.','.','6','.','.','.','3'},
		{'4','.','.','8','.','3','.','.','1'},
		{'7','.','.','.','2','.','.','.','6'},
		{'.','6','.','.','.','.','2','8','.'},
		{'.','.','.','4','1','9','.','.','5'},
		{'.','.','.','.','8','.','.','7','9'},
	}
	solveSudoku(board)
	fmt.Println(board)
}

func solveSudoku(board [][]byte) {
	solve(board)
}

func solve(board [][]byte) bool {
	for i := 0; i < len(board); i++ {
		for j := 0; j < len(board[0]); j++ {
			if board[i][j] != '.' {
				continue
			}
			for s := '1'; s <= '9'; s++ {
				if !valid(i, j, board, byte(s)) {
					continue
				}
				board[i][j] = byte(s)
				if solve(board) {
					return true
				}
				board[i][j] = '.'
			}
			return false
		}
	}
	return true
}

func valid(row, col int, board [][]byte, s byte) bool {
	for i := 0; i < len(board); i++ {
		if board[i][col] == s {
			return false
		}
	}
	for _, b := range board[row] {
		if b == s {
			return false
		}
	}

	c := col / 3.0 * 3.0
	r := row / 3.0 * 3.0
	for i := r; i < r + 3; i++ {
		for j := c; j < c + 3; j++ {
			if board[i][j] == s {
				return false
			}
		}
	}
	return true
}
