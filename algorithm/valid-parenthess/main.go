package main

import (
	"fmt"
	"strconv"
)

type stack struct {
	base []int32
	top  int
}

func NewStack(defaultSize int) *stack {
	if defaultSize == 0 {
		defaultSize = 64
	}
	return &stack{base: make([]int32, 0, defaultSize)}
}

func (s *stack) isEmpty() bool {
	return s.top == 0
}

func (s *stack) pop() int32 {
	if s.top == 0 {
		r, _ := strconv.Atoi("")
		return int32(r)
	}
	s.top--
	r := s.base[s.top]
	return r
}

func (s *stack) view() {
	fmt.Printf("%#v\n", s.base[:s.top])
}

func (s *stack) push(d int32) {
	if len(s.base) > s.top {
		s.base[s.top] = d
	} else {
		s.base = append(s.base, d)
	}
	s.top++
}

func isValid(s string) bool {
	leftMap := map[int32]int32{
		93:  91,
		41:  40,
		125: 123,
	}
	sk := NewStack(0)
	for _, i2 := range s {
		right, ok := leftMap[i2]
		if !ok {
			sk.push(i2)
			continue
		}
		if right != sk.pop() {
			return false
		}
	}
	return sk.isEmpty()
}

func main() {
	fmt.Println(isValid("[({})][]}"))
	fmt.Println(isValid("[({})]{[]}(){}{{{{}}}}()"))
}
