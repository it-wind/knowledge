### 字符集&字符编码

#### 1.字符集
```text
>字符（Character）是各种文字和符号的总称。
>包括各国家文字、标点符号、图形符号、数字等。字符集（Character set）就是多个字符的集合。
>字符集种类较多，每个字符集包含的字符个数不同。
```

#### 2.字符编码
```text
>计算机中只能存储二进制数据。
>将一个字符映射成一个二进制数据的过程也叫做 编码 。
>将一个二进制数据映射到一个字符的过程叫做 解码 。
如:
  'a' -> 00000001 (十六进制：0x01)
  'b' -> 00000010 (十六进制：0x02)
  'A' -> 00000011 (十六进制：0x03)
  'B' -> 00000100 (十六进制：0x04)
```

#### 3.ASCII码
```text
>American Standard Code for Information Interchange，美国信息互换标准编码。
>基于罗马字母表的一套电脑编码系统。
>主要用户显示现代英语和其他西欧语言，是最通用的单字节编码系统。
>7位（bits）表示一个字符，共128字符，字符值0~127。
```

#### 4.非ASCII编码的诞生
```text
英语用128个符号编码就够, 但是其它语言比如汉字就多达10万左右。
ASCII无法满足时，其它国家就会研制自己国家的字符编码。
```

#### 5.GB2312编码
```text
>《信息交换用汉字编码字符集》中国国家标准总局1980年发布
>GB2312编码适用于汉字处理、汉字通信等系统之间的信息交换。
通行于中国大陆、新加坡等地也采用此编码。
中国大陆几乎所有的中文系统和国际化的软件都支持GB2312。
```

#### 6.乱码
```text
>当读和写、发和收的编码不一致时，就会出现乱码。
>一种解决方案是让两端使用的编码保持一致，局限性是无法跨语言。
>如果有一种编码，将世界上所有的符号都纳入其中。
每一个符号都给予一个独一无二的编码，那么乱码问题就会消失
```

#### 7.Unicode
```text
>Unicode 一个很大的集合，现在的规模可以容纳100多万个符号。
>它只规定了符号的二进制代码，却没有规定这个二进制代码应该如何存储。
>如:
1.Unicode编码 4e2d 二进制15位(100111000101101)，这个符号表示需要2个字节，怎么判断这个编码代表几个字符呢？
```

#### 8.UTF-8
```text
>UTF-8编码是Unicode码实现方式之一
>UTF-8 最大的一个特点，就是它是一种变长的编码方式。
它可以使用1~4个字节表示一个符号，根据不同的符号而变化字节长度。
>UTF-8 的编码规则:
1.对于单字节的符号，字节的第一位设为0，后面7位为这个符号的 Unicode 码。
因此对于英语字母，UTF-8 编码和 ASCII 码是相同的。
2.对于n字节的符号（n > 1），第一个字节的前n位都设为1，第n + 1位设为0，后面字节的前两位一律设为10。
剩下的没有提及的二进制位，全部为这个符号的 Unicode 码。
>如果一个字节的第一位是0，则这个字节单独就是一个字符；
如果第一位是1，则连续有多少个1，就表示当前字符占用多少个字节。
```

#### 9.Little endian 和 Big endian
```text
>以"中"字为例，Unicode码为4E2D，需要2字节存储。
4E2D  Big endian 存储方式
2D4E  Little endian 存储方式

>第一字节在前就是 Big endian 存储方式。第一字节在后就是 Little endian 存储方式
>Unicode 规范定义文件开头前2个字节存储"零宽度非换行空格"，用 FEFF 表示。
FE FF 表示使用的是 Big endian 存储方式
FF FE 表示使用的是 Little endian 存储方式
```
