package main

import (
	"fmt"
	"math/rand"
	"sync/atomic"
	"time"
)

// DefaultMaxSpeed 用于进入拥塞避免的最大窗口
const DefaultMaxSpeed = 32

const (
	SenderStateSlow     = 0 //慢启动
	SenderStateAvoid    = 1 //拥塞避免
	SenderStateOverTime = 2 //网络阻塞
	SenderStateRecovery = 3 //快速恢复
)

//Sender 发送方
type Sender struct {
	Id       string        //发送方ID
	Seq      uint64        //消息序列号
	Speed    time.Duration //cwnd 拥塞窗口，假设接收方窗口始终大于拥塞窗口
	MaxSpeed time.Duration //ssthresh slow start threshold
	State    uint8         //当前发送方网络状态
}

//Channel 网络通道
type Channel struct {
	Data  chan string
	Speed time.Duration //允许通过消息的发送窗口总大小
}

//用于分配发送方的Next-ID
var senderId uint64 = 0

func main() {
	//创建一个网络通道，最大同时存在发送方窗口总和：128，超过会发生超时
	ch := NewChannel(128)

	//创建发送方
	go RunSender(ch)
	go RunSender(ch)

	time.Sleep(time.Hour)
}

func NewChannel(speed time.Duration) *Channel {
	ch := &Channel{
		Data:  make(chan string, 10),
		Speed: speed,
	}
	go Receive(ch)
	return ch
}

func RunSender(ch *Channel) {
	s := Sender{
		Speed:    1,
		Id:       fmt.Sprintf("sender-%d", atomic.AddUint64(&senderId, 1)),
		MaxSpeed: DefaultMaxSpeed,
		State:    SenderStateSlow,
	}

	//0~3秒的随机定时器
	tk := time.NewTicker(time.Millisecond * time.Duration(rand.Intn(3000)))
	fmt.Println(s.Id, "开始启动")
	for {
		select {
		//模拟0～3秒内有丢包发生，导致进入快速恢复
		case <-tk.C:
			tk.Reset(time.Millisecond * time.Duration(rand.Intn(3000)))
			s.Speed /= 2
			s.MaxSpeed = s.Speed
			s.Speed += 3
			s.State = SenderStateRecovery
			fmt.Println(s.Id, "发生丢包，进入快速恢复", "speed:", s.Speed, "max_speed:", s.MaxSpeed)
		case ch.Data <- fmt.Sprintf("%s:%d", s.Id, atomic.AddUint64(&s.Seq, 1)):
			//目前网络无阻塞
			if s.Speed > s.MaxSpeed {
				//拥塞避免
				s.Speed++
				s.State = SenderStateAvoid
				fmt.Println(s.Id, "进入拥塞避免", "speed:", s.Speed, "max_speed:", s.MaxSpeed)
			} else {
				//慢启动中
				s.Speed *= 2
				s.State = SenderStateSlow
				fmt.Println(s.Id, "进入慢启动", "speed:", s.Speed, "max_speed:", s.MaxSpeed)
			}
		default:
			//网络阻塞 - 进入慢启动
			s.MaxSpeed = s.Speed / 2
			fmt.Println(s.Id, "进入网络阻塞", "speed:", s.Speed, "max_speed:", s.MaxSpeed)
			s.Speed = 1
			s.State = SenderStateOverTime
		}
		time.Sleep(time.Second * 1 / s.Speed)
	}
}

//Receive 网络通道处理消息，用来限制最大发送方窗口总和
func Receive(ch *Channel) {
	for {
		time.Sleep(time.Second * 1 / ch.Speed)
		<-ch.Data
		//fmt.Println(<-ch.Data)
	}
}
