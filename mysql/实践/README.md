### 普通索引和唯一索引的选择
- 优先满足业务需要
- 如果当前表有且只有一个索引，且该索引内容不重复，可设置为唯一索引，反之就设置为普通索引
  - 唯一索引不能使用change buffer

### 字符串类型添加索引
- 整个字段定义为索引
  - 索引占用空间大
- 字段前M个字符定义为索引（前缀索引）
  - 索引占用空间小
  - 会损失区分度，导致扫描行数变多
  - 不能使用覆盖索引
  - 需合理的定义前缀的长度，控制合理的损失区分度的比例

### 不带where条件的count(*)效率
- MyISAM 直接返回行数
- InnoDB MVCC的存在，要扫描索引树才能确认返回行数
  - count(*)
    - 优先使用最小的索引树进行遍历，减少扫描行数
  - count(1) & count(id)
    - 遍历主键索引，由于主键索引叶子节点存储的是数据，主键索引一般不是最小的索引树
    - count(id) 获取id的值，累加行数
    - count(1) 不获取值，server层直接累加行数
  - count(字段)
    - 如果字段有索引，会从该索引树遍历，判断是否为null，然后累加行数
    - 如果字段没有索引，遍历主键索引，获取该字段判断是否为null，然后累加行数
  
总结：
- count(字段)有索引
  - count(*) > count(字段) > count(1) > count(id)
- count(字段)无索引
  - count(*) > count(1) > count(id) > count(字段)

### 常见优化器不使用索引的情况
```mysql
##流水日志表
CREATE TABLE `tradelog` (
   `id` int(11) NOT NULL,
   `tradeid` varchar(32) DEFAULT NULL,
   `operator` int(11) DEFAULT NULL,
   `t_modified` datetime DEFAULT NULL,
   PRIMARY KEY (`id`),
   KEY `tradeid` (`tradeid`),
   KEY `t_modified` (`t_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


##对于检索列使用函数mysql优化器放弃使用索引搜索功能
select count(*) from tradelog where month(t_modified)=7;

##隐式类型转化
##如果结果是1，则规则是把字符串转化为数字
##如果结果是0，则规则是把数字转为字符串
select "10" > 9; ##结果为1， 说明字符串和数字比较，mysql会把字符串转换成数字

##tradeid 为varchar类型，因此mysql会把tradeid全部都转换为数字和110717进行比较
##因此mysql优化器放弃使用索引搜索功能
select * from tradelog where tradeid=110717; ## 相当于 select * from tradelog where CAST(tradid AS signed int) = 110717;

##id 为int类型，mysql会把"83126"转换为83126也即是字符串转int类型
##因此mysql优化器使用索引搜索功能
select * from tradelog where id="83126";

##隐式编码转换
CREATE TABLE `trade_detail` (
   `id` int(11) NOT NULL,
   `tradeid` varchar(32) DEFAULT NULL,
   `trade_step` int(11) DEFAULT NULL, /*操作步骤*/
   `step_info` varchar(32) DEFAULT NULL, /*步骤信息*/
   PRIMARY KEY (`id`),
   KEY `tradeid` (`tradeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

##tradelog表默认字符集utf8mb4
##trade_detail表默认字符集utf8
select d.* from tradelog l, trade_detail d where d.tradeid=l.tradeid and l.id=2;
```
通过explain分析
![img_3.png](img_3.png)
- tradelog表为驱动表，且tradelog.id=2使用了索引
- trade_detail表为被驱动表，且trade_detail.tradeid进行了全表扫描
```mysql
##utf8mb4是utf8的超集，utf8mb4和utf8字符集字段进行比较时，会把utf8字符集转换为utf8mb4字符集
##如下：
select d.* from tradelog l, trade_detail d where  CONVERT(d.traideid USING utf8mb4)=l.tradeid and l.id=2;

##两种方法可以使用上索引
##1、把两个表的字符集统一
##2、转换tradelog.traideid为utf8
##如下：
select d.* from tradelog l , trade_detail d where d.tradeid=CONVERT(l.tradeid USING utf8) and l.id=2;
```