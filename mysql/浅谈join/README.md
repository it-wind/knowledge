```mysql
CREATE TABLE `t1` (
  `id` int(11) NOT NULL,
  `a` int(11) DEFAULT NULL,
  `b` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `a` (`a`)
) ENGINE=InnoDB;

create table t2 like t1;
```

### 浅谈NLJ算法
被驱动表使用上索引：
```mysql
#straight_join 优化器只会按照我们指定的方式去 join
explain select * from t1 straight_join t2 on (t1.a=t2.a);
```

![img.png](img.png)

可以看出被驱动表t2中的a索引被使用上，流程如下：
- 1、从表 t1 中读入一行数据 i；
- 2、从数据行 i 中，取出 a 字段到表 t2 里去查找；
- 3、取出表 t2 中满足条件的行 j，跟 i 组合为部分结果集；
- 4、重复执行步骤 1 到 3，直到表 t1 的末尾循环结束。

很类似两层for循环。所以我们称之为“Index Nested-Loop Join”，简称 NLJ。

- 这里我们粗略的认为b+树的检索时间复杂度是以2为底的 O(logN)
- 假设驱动表查找的数据是N行，被驱动表有M行数据，总体检索数据的时间负责度：N+2N*logM
  -  可以看出N的大小很关键，所以应该让小表做驱动表

### 浅谈BNL算法
被驱动表无法使用索引：
```mysql
#t2表中b字段并未建立索引
select * from t1 straight_join t2 on (t1.a=t2.b);
```

在不做优化的情况下，Simple Nested-Loop Join：
- 假设驱动表查找的数据是N行，被驱动表有M行数据，总体检索数据的时间负责度：N*M （效率极差）

引入join_buffer情况下，Block Nested-Loop Join：
- 把表 t1 的数据读入线程内存 join_buffer 中，由于我们这个语句中写的是 select *，因此是把整个表 t1 放入了内存；
- 扫描表 t2，把表 t2 中的每一行取出来，跟 join_buffer 中的数据做对比，满足 join 条件的，作为结果集的一部分返回。
- 假设驱动表查找的数据是N行，被驱动表有M行数据，总体检索数据的时间负责度：N*M （效率较差）

*如果join_buffer满了，就分段装入，整体查询流程不变

总体建议是始终使用`小表`作为驱动表，确保被驱动表充分利用索引查询

`小表`的衡量标准：需要被检索出来的数据范围，而不是表中的所有数据

### Multi-Range Read 优化
```mysql
select * from t1 where a>=1 and a<=100;
```
- 通过索引a查询到主键ID
- 再通过主键ID回表

可以看出根据主键ID查询数据是一个个的随机读IO

引入MRR优化：
- 根据索引 a，定位到满足条件的记录，将 id 值放入 read_rnd_buffer 中 ;
- 将 read_rnd_buffer 中的 id 进行递增排序；
- 排序后的 id 数组，依次到主键 id 索引中查记录，并作为结果返回。

如果 read_rnd_buffer就分段装入，查询流程不变

MRR 能够提升性能的核心在于，这条查询语句在索引 a 上做的是一个范围查询（也就是说，这是一个多值查询），可以得到足够多的主键 id。这样通过排序以后，再去主键索引查数据，才能体现出“顺序性”的优势。
如果你想要稳定地使用 MRR 优化的话，需要设置set optimizer_switch="mrr_cost_based=off"

### Batched Key Access
