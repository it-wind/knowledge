### RedoLog
- redo log 是物理日志，记录的是“在某个数据页上做了什么修改”
- 直接修改磁盘中的数据，随机写IO成本过高，先顺序写日志及修改内存中的数据，在空闲期刷回磁盘(WAL)
- redolog是固定大小，比如可以配置为一组 4 个文件，每个文件的大小是 1GB，从头开始写，写到末尾就又回到开头循环写
![img_1.png](img_1.png)
- write pos 是当前记录的位置，一边写一边后移
- checkpoint 是当前要擦除的位置，也是往后推移并且循环, 擦除记录前要把记录更新到数据文件
- write pos 到 checkpoint 为可用空间

### Binlog
binlog 是逻辑日志，记录的是这个语句的原始逻辑，比如“给 ID=2 这一行的 c 字段加 1 ”

### 结合update语句的执行流程看redolog和binlog
mysql> create table T(ID int primary key, c int);
mysql> update T set c=c+1 where ID=2;

深绿色：Server层; 浅绿色：InnoDB存储引擎层;
![img.png](img.png)
- binlog在Server层
- redolog在InnoDB存储引擎层
- binlog和redolog的写是两阶段的，保证两者间的一致性

### 3、[事务隔离](../事务隔离/README.md)
