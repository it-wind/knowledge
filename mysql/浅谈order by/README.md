```mysql
CREATE TABLE `t` (
  `id` int(11) NOT NULL,
  `city` varchar(16) NOT NULL,
  `name` varchar(16) NOT NULL,
  `age` int(11) NOT NULL,
  `addr` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `city` (`city`)
) ENGINE=InnoDB;
```

select city,name,age from t where city='杭州' order by name limit 1000 ;

1、全字段排序算法 (优先被选择)
- 1.初始化 sort_buff ：用于存放排序数组的内存区域
- 2.从索引city 找到第一个条满足 city=杭州 条件的主键ID
- 3.根据主键ID从主键索引中获取name、city、age值放入到sort_buff
- 4.从索引 city 取下一个记录的主键 id
- 5.重复步骤 3、4 直到 city 的值不满足查询条件为止
- 6.对sort_buff中的数据按照name做快速排序
- 7.返回前1000条数据

如图：
![img.png](img.png)
如果sort_buffer_size小于需要排序的数据集，就需要用外部排序

例如：需要排序的数据是5M，sort_buffer_size = 512KB
- 数据会被切割成 5M/512KB = 12份，均单独排序，排序后存储到外部的12个临时文件中
- 再对12个临时文件使用归并排序，最后获取前1000条数据返回

临时文件过多，排序的性能必然下降
max_length_for_sort_data，是 MySQL 中专门控制用于排序的行数据的长度的一个参数。它的意思是，如果单行的长度超过这个值，MySQL 就认为单行太大，要换一个算法

2、rowid 排序算法 (只有要排序的列（即 name 字段）和主键 id)
- 1.初始化 sort_buff ：用于存放排序数组的内存区域
- 2.从索引city 找到第一个条满足 city=杭州 条件的主键ID
- 3.根据主键ID从主键索引中获取name、id值放入到sort_buff
- 4.从索引 city 取下一个记录的主键 id
- 5.重复步骤 3、4 直到 city 的值不满足查询条件为止
- 6.对sort_buff中的数据按照name做快速排序
- 7.取前1000行数据，并按照id进行回表取出city、name、age后返回

如图:
![img_1.png](img_1.png)

3、所有order by都需要排序吗
- 需要排序是因为数据本身是无序的，如果能够保证从 city 这个索引上取出来的行，就是按照 name 递增排序的话，就可以不用再排序
- 使用联合索引 alter table t add index city_user(city, name);
