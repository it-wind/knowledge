package binary_tree

import (
	"fmt"
	"gitee.com/it-wind/knowledge/course/data-struct/deque"
)

func Bfs() {
	tree := NewTree()
	data := []int{15,8,22,5,11,18,26}

	for _, datum := range data {
		tree.Insert(datum)
	}

	q := deque.NewQueue()
	q.LPush(tree.root)

	for {
		e, n := q.RPop()
		if e == false {
			break
		}
		n1, e1 := n.(*node)
		if e1 == false {
			continue
		}
		println(n1.key)
		if n1.left != nil {
			q.LPush(n1.left)
		}
		if n1.right != nil {
			q.LPush(n1.right)
		}
	}
}

func LevelBfs() {
	tree := NewTree()
	data := []int{15,8,22,5,11,18,26}

	for _, datum := range data {
		tree.Insert(datum)
	}

	q := deque.NewQueue()
	q.LPush(tree.root)

	levelArr := make([][]int, 0)
	for {
		qLen := q.Len()
		if qLen == 0 {
			break
		}
		arr := make([]int, qLen)
		for i:=0; i<qLen; i++ {
			e, n := q.RPop()
			if e == false {
				break
			}
			n1, e1 := n.(*node)
			if e1 == false {
				continue
			}
			arr[i] = n1.key
			if n1.left != nil {
				q.LPush(n1.left)
			}
			if n1.right != nil {
				q.LPush(n1.right)
			}
		}
		levelArr = append(levelArr, arr)
	}

	for i, arr := range levelArr {
		fmt.Println(i, arr)
	}
}