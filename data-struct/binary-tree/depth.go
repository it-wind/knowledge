package binary_tree

import "math"

func MaxDepth(node2 *node) int {
	if node2 == nil {
		return 0
	}
	return 1 + int(math.Max(float64(MaxDepth(node2.left)), float64(MaxDepth(node2.right))))
}

func MinDepth(node2 *node) int {
	if node2 == nil {
		return 0
	}
	if node2.left == nil || node2.right == nil {
		return 1
	}
	return 1 + int(math.Min(float64(MinDepth(node2.left)), float64(MinDepth(node2.right))))
}
