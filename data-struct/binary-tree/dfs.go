package binary_tree

import "gitee.com/it-wind/knowledge/course/data-struct/deque"

func RecursionDfs(node2 *node) {
	if node2 == nil {
		return
	}
	println(node2.key)
	RecursionDfs(node2.left)
	RecursionDfs(node2.right)
}

func Dfs() {
	tree := NewTree()
	data := []int{15,8,22,5,11,18,26}

	for _, datum := range data {
		tree.Insert(datum)
	}

	q := deque.NewQueue()
	q.LPush(tree.root)

	for {
		e, n := q.LPop()
		if e == false {
			break
		}
		n1, e1 := n.(*node)
		if e1 == false {
			continue
		}
		println(n1.key)
		if n1.right != nil {
			q.LPush(n1.right)
		}
		if n1.left != nil {
			q.LPush(n1.left)
		}
	}
}