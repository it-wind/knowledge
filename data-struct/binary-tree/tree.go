package binary_tree

type node struct {
	left  *node
	right *node
	key   int
}

type tree struct {
	root *node
}

func NewTree() *tree {
	return &tree{}
}

const (
	MID = iota
	LEFT
	RIGHT
)

func (t *tree) Insert(k int) bool {
	if t.root == nil {
		t.root = &node{key: k}
		return true
	}

	n := t.root
	for {
		if n.key == k {
			return false
		}
		if k > n.key {
			if n.right == nil {
				n.right = &node{key: k}
				return true
			}
			n = n.right
			continue
		}

		if n.left == nil {
			n.left = &node{key: k}
			return true
		}
		n = n.left
	}
}

func (t *tree) Del(k int) bool {
	if t.root == nil {
		return false
	}

	//查找删除节点
	var direction int = MID
	var parent *node = t.root
	var current *node = t.root
	for {
		if current.key == k {
			break
		}
		if current.key < k {
			if current.right == nil {
				return false
			}
			parent = current
			current = current.right
			direction = RIGHT
		}
		if current.left == nil {
			return false
		}
		parent = current
		current = current.left
		direction = LEFT
	}

	var replaceNode *node
	if current.left == nil {
		if current.right == nil {
			//叶子节点, 直接删除
			replaceNode = nil
		}
		//current.right
		replaceNode = current.right
	} else if current.right == nil {
		//current.left
		replaceNode = current.left
	} else {
		//两个子节点都不为空，寻找后继节点
		var rightMinCur *node = current.right
		var rightMinPar *node = current
		var rightMinDir int = RIGHT
		for rightMinCur.left != nil {
			rightMinDir = LEFT
			rightMinPar = rightMinCur
			rightMinCur = rightMinCur.left
		}

		var rightMinRep *node
		if rightMinCur.right != nil {
			//后继节点有右子节点, 后继节点的右子节点和其父节点关联
			rightMinRep = rightMinCur.right
		}

		if rightMinDir == LEFT {
			rightMinPar.left = rightMinRep
		} else {
			rightMinPar.right = rightMinRep
		}
		replaceNode = rightMinCur
	}

	//被删除节点的父节点和替换节点产生关联
	if direction == MID {
		//被删除节点是父节点，改变替换节点为根节点
		t.root = replaceNode
	} else if direction == LEFT {
		parent.left = replaceNode
	} else {
		parent.right = replaceNode
	}
	replaceNode.left = current.left
	replaceNode.right = current.right
	return true
}