package binary_tree

import (
	"fmt"
	"testing"
)

func TestTree_Del(t *testing.T) {
	tree := NewTree()
	data := []int{15,8,22,5,11,18,26}

	for _, datum := range data {
		tree.Insert(datum)
	}

	tree.Del(15)

	if tree.root.key != 18 {
		t.Error("删除实现错误")
	}
}

func TestLevelBfs(t *testing.T) {
	LevelBfs()
}

func TestMaxAndMinDepth(t *testing.T) {
	tree := NewTree()
	data := []int{15,8,22,5,11,26,28}

	for _, datum := range data {
		tree.Insert(datum)
	}

	fmt.Println("max_depth:", MaxDepth(tree.root))
	fmt.Println("min_depth:", MinDepth(tree.root))
}

func TestRecursionDfs(t *testing.T) {
	tree := NewTree()
	data := []int{15,8,22,5,11,26,28}

	for _, datum := range data {
		tree.Insert(datum)
	}

	RecursionDfs(tree.root)
}