package deque

import "sync"

type node struct {
	prev *node
	next *node
	data interface{}
}

type queue struct {
	sync.RWMutex
	head *node
	tail *node
	len int
}

func NewQueue() *queue {
	return &queue{}
}

func (q *queue) LPush(d interface{}) {
	q.Lock()
	defer q.Unlock()

	q.len ++
	if q.head == nil {
		q.head = &node{data: d}
		q.tail = q.head

		return
	}

	n := &node{
		data: d,
		next: q.head,
	}
	q.head.prev = n
	q.head = n
}

func (q *queue) RPush(d interface{}) {
	q.Lock()
	defer q.Unlock()

	q.len ++
	if q.head == nil {
		q.head = &node{data: d}
		q.tail = q.head

		return
	}

	n := &node{
		data: d,
		prev: q.tail,
	}
	q.tail.next = n
	q.tail = n
}

func (q *queue) LPop() (exists bool, d interface{}) {
	q.Lock()
	defer q.Unlock()

	if q.head == nil {
		return
	}

	exists = true
	d = q.head.data
	q.len --
	if q.head == q.tail {
		q.head = nil
		q.tail = nil
		return
	}

	q.head = q.head.next
	q.head.prev = nil
	return
}

func (q *queue) RPop() (exists bool, d interface{}) {
	q.Lock()
	defer q.Unlock()

	if q.head == nil {
		return
	}

	exists = true
	d = q.tail.data
	q.len --
	if q.head == q.tail {
		q.head = nil
		q.tail = nil
		return
	}

	q.tail = q.tail.prev
	q.tail.next = nil
	return
}

func (q *queue) Len() int {
	q.RLock()
	defer q.RUnlock()
	return q.len
}