package deque

import (
	"math/rand"
	"testing"
)

func TestQueue_LPush_RPop(t *testing.T) {
	q := NewQueue()
	n := [10000]int{}
	for i := 0; i < 10000; i++ {
		randN := rand.Int()
		n[i] = randN
		q.LPush(randN)
	}

	for i := 0; i < 10000; i++ {
		e, c := q.RPop()
		if e == false {
			t.Error("数据丢失")
		}
		if c != n[i] {
			t.Error("数据不一致")
		}
	}
}

func TestQueue_RPush_LPop(t *testing.T) {
	q := NewQueue()
	n := [10000]int{}
	for i := 0; i < 10000; i++ {
		randN := rand.Int()
		n[i] = randN
		q.RPush(randN)
	}

	for i := 0; i < 10000; i++ {
		e, c := q.LPop()
		if e == false {
			t.Error("数据丢失")
		}
		if c != n[i] {
			t.Error("数据不一致")
		}
	}
}
