package red_black_tree

type Node struct {
	parent *Node
	left *Node
	right *Node
	data int
	color uint8
}

type RBTree struct {
	root *Node
}

const (
	RED = iota
	BlACK
)

func NewRBTree() *RBTree {
	return &RBTree{nil}
}

func (t *RBTree) Insert(data int) {
	if t.root == nil {
		t.root = &Node{
			data: data,
			color: BlACK,
		}
		return
	}
	root := t.root.Insert(data)
	if root != nil {
		t.root = root
	}
}

func (n *Node) Uncle() *Node {
	if n.parent.IsLeft() {
		return n.parent.right
	}
	return n.parent.left
}

func (n *Node) IsLeft() bool {
	if n.parent == nil {
		return false
	}
	return n.parent.left == n
}

func (n *Node) IsColorBlack() bool {
	if n == nil {
		return true
	}
	return n.color == BlACK
}

func (n *Node) SetColorRed() {
	if n != nil {
		n.color = RED
	}
}

func (n *Node) SetColorBlack() {
	if n != nil {
		n.color = BlACK
	}
}

func (n *Node) LeftRotate() (root *Node) {
	if n == nil || n.right == nil {
		return
	}

	//先拿到被旋转节点的右子节点
	right := n.right
	//如果被旋转节点存在父节点，让其父节点和其右子节点相互关联
	if n.parent == nil {
		root = right
		root.parent = nil
	} else {
		if n.IsLeft() {
			n.parent.left = right
		} else {
			n.parent.right = right
		}
		right.parent = n.parent
	}

	//让被旋转的节点的右子节点指针和其右子节点的左子节点相互关联
	n.right = right.left
	if right.left != nil {
		right.left.parent = n
	}

	//让被旋转的节点的右子节点和被旋转节点互换
	right.left = n
	n.parent = right
	return
}

func (n *Node) RightRotate() (root *Node) {
	if n == nil || n.left == nil {
		return
	}

	left := n.left
	if n.parent == nil {
		root = left
		root.parent = nil
	} else {
		if n.IsLeft() {
			n.parent.left = left
		} else {
			n.parent.right = left
		}
		left.parent = n.parent
	}

	n.left = left.right
	if left.right != nil {
		left.right.parent = n
	}

	left.right = n
	n.parent = left
	return
}

func (n *Node) InsertFix() (root *Node) {
	//父节点为空的节点即是根节点
	//根节点颜色必须是黑色
	if n.parent == nil {
		n.color = BlACK
		return
	}

	//父节点颜色是黑色无需操作
	if n.parent.IsColorBlack() {
		return
	}
	//父节点为红色

	//叔叔节点是红色
	if !n.Uncle().IsColorBlack() {
		//父亲和叔叔变成黑色
		n.parent.SetColorBlack()
		n.Uncle().SetColorBlack()
		//祖父变成红色
		n.parent.parent.SetColorRed()
		//修复祖父
		return n.parent.parent.InsertFix()
	}
	//叔叔节点为黑色

	if n.parent.IsLeft() {
		if n.IsLeft() {
			//父亲是左节点，当前是左节点
			//父亲变黑色
			n.parent.SetColorBlack()
			//祖父变红色
			n.parent.parent.SetColorRed()
			//祖父右旋
			return n.parent.parent.RightRotate()
		}
		//父亲是左节点，当前是右节点
		p := n.parent
		root = n.parent.LeftRotate()
		root1 := p.InsertFix()
		if root1 != nil {
			root = root1
		}
		return
	}

	if n.IsLeft() {
		//父亲是右节点，当前是左节点
		p := n.parent
		root = n.parent.RightRotate()
		root1 := p.InsertFix()
		if root1 != nil {
			root = root1
		}
		return
	}
	//父亲是右节点，当前是右节点
	//父亲变黑色
	n.parent.SetColorBlack()
	//祖父变红色
	n.parent.parent.SetColorRed()
	//祖父左旋
	return n.parent.parent.LeftRotate()
}

func (n *Node) Insert(data int) (root *Node) {
	z := &Node{
		data: data,
		color: RED,
	}
	t := n
	for t != nil {
		if data == t.data {
			return
		}
		if data > t.data {
			if t.right == nil {
				t.right = z
				z.parent = t
				return z.InsertFix()
			}
			t = t.right
		} else {
			if t.left == nil {
				t.left = z
				z.parent = t
				return z.InsertFix()
			}
			t = t.left
		}
	}
	return
}