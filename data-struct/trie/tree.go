package trie

type Trie struct {
	isWord bool
	child [26]*Trie
}


func Constructor() Trie {
	return Trie{}
}


func (this *Trie) Insert(word string)  {
	if word == "" {
		return
	}
	u := word[0] - 'a'
	if this.child[u] == nil {
		this.child[u] = &Trie{}
	}
	if len(word) == 1 {
		this.child[u].isWord = true
	}
	this.child[u].Insert(word[1:])
}


func (this *Trie) Search(word string) bool {
	if word == "" {
		return false
	}
	u := word[0] - 'a'
	if this.child[u] == nil {
		return false
	}
	if len(word) == 1 && this.child[u].isWord {
		return true
	}
	return this.child[u].Search(word[1:])
}


func (this *Trie) StartsWith(prefix string) bool {
	if prefix == "" {
		return false
	}
	u := prefix[0] - 'a'
	if this.child[u] == nil {
		return false
	}
	if len(prefix) == 1 {
		return true
	}
	return this.child[u].StartsWith(prefix[1:])
}

